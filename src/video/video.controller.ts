import {
  Controller,
  Post,
  Body,
  Req,
  HttpException,
  HttpStatus,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiUseTags, ApiConsumes, ApiImplicitFile, ApiResponse, ApiBearerAuth} from '@nestjs/swagger';

import { VideoService } from './video.service';

import { FileLinkDto } from './dto/filelink.dto';
import { UploadFileDto } from './dto/uploadfile.dto';
import { VideoStreamsDto } from './dto/videostreams.dto';

import { multerOptions } from '../common/services/video-upload.service';

import { LOCAL_FILE, REMOTE_FILE } from '../common/constants/subtitle.constanst';
import { REG_URL_PATTERN } from '../common/constants/paths.constants';
import { YoutubeDownload } from '../common/decorators/youtube-download.decorator';
import { AuthGuard } from '@nestjs/passport';

@Controller('video')
@ApiUseTags('video')
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class VideoController {
  constructor(
    private readonly videoService: VideoService,
  ){}

  @Post('/upload')
  @UseInterceptors(FileInterceptor('file', multerOptions))
  @ApiOperation({ title: 'Upload video file' })
  @ApiResponse({ status: 200, type: VideoStreamsDto })
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true, description: 'Video file for project' })
  async uploadFile(@Req() req, @UploadedFile() file: UploadFileDto): Promise<VideoStreamsDto> {
    if (req.fileValidationError) {
      throw new HttpException(req.fileValidationError, HttpStatus.BAD_REQUEST);
    }

    if (!file) {
      throw new HttpException('Can\'t upload file with undefined format', HttpStatus.BAD_REQUEST);
    }
    return this.videoService.makeVideoStreams(file.path, LOCAL_FILE, req.body.id);
  }

  @Post('/remote-video')
  @YoutubeDownload()
  @ApiOperation({ title: 'Open remote video file' })
  @ApiResponse({ status: 200, type: VideoStreamsDto })
  async create(@Body() file: FileLinkDto): Promise<VideoStreamsDto> {
    if (!file.downloadedVideoPath) {
      const pattern = new RegExp(REG_URL_PATTERN, 'i');
      if (!pattern.test(file.path)) {
        throw new HttpException('URL not valid', HttpStatus.BAD_REQUEST);
      }
    }

    const filePath = file.downloadedVideoPath || file.path;
    const FILE_LOCATION = (file.downloadedVideoPath && LOCAL_FILE) || REMOTE_FILE;

    return this.videoService.makeVideoStreams(filePath, FILE_LOCATION, file.socketId);
  }
}