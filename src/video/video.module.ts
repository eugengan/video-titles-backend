import { Module } from '@nestjs/common';
import { VideoController } from './video.controller';
import { VideoService } from './video.service';
import { VideoGateway } from './video.gateway';
import { FileStoreService } from '../common/services/file-store.service';
import { YoutubeUploadService } from '../common/services/youtubeDownload-service';

@Module({
  imports: [],
  controllers: [VideoController],
  providers: [
    VideoService,
    VideoGateway,
    FileStoreService,
    YoutubeUploadService,
  ],
  exports: [VideoService],
})
export class VideoModule { }