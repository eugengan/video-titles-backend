import { IsString, IsArray, ValidateNested } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Metadata } from './metadata.dto';

export class VideoStreamsDto {
  @IsString() @ApiModelProperty() readonly path: string;
  @ValidateNested() @ApiModelProperty() readonly metadata: Metadata;
  @IsArray() @ApiModelProperty() readonly audioFiles: object;
}
