import { IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class FileLinkDto {
  @IsString() @ApiModelProperty() readonly path: string;
  @IsString() @ApiModelProperty() readonly socketId: string;
  @ApiModelProperty() readonly downloadedVideoPath: string;
}
