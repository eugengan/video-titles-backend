import { IsNumber, IsString, IsArray } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class Metadata {
  @IsArray() @ApiModelProperty() readonly streams?: Array<Streams>;
}

export class Streams {
  @IsNumber() @ApiModelProperty() readonly index?: number;
  @IsString() @ApiModelProperty() readonly codec_type?: string;
  @IsString() @ApiModelProperty() readonly codec_name?: string;
}