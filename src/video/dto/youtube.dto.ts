import { Info } from 'youtube-dl';

export interface YoutbeMetadataDto extends Info {
  readonly ext: string;
}
