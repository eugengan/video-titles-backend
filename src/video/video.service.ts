import { Injectable } from '@nestjs/common';
import { readdir, ensureDir } from 'fs-extra';
import { flatten } from 'lodash';
import { getMetadata } from '../common/utils';
import { ffmpegStatic as ffmpeg  } from '../common/utils';
import { v4 as uuid } from 'uuid';

import { VideoStreamsDto } from './dto/videostreams.dto';
import { Metadata } from './dto/metadata.dto';

import { SERVER_PATH_TEMP, PATH_TEMP } from '../common/constants/paths.constants';
import { AUDIO_EXT } from '../common/constants/subtitle.constanst';
import { VideoGateway } from './video.gateway';

@Injectable()
export class VideoService {
 constructor(private readonly vg: VideoGateway)
  { }
 async makeVideoStreams(path: string, from: string, socketId: number | string): Promise<VideoStreamsDto> {

  const dirId = uuid();
  const dir = `${PATH_TEMP}/${dirId}`;
  await ensureDir(dir);
  this.vg.server.sockets.to(socketId).emit('log', 'success');

  const metadata = await getMetadata(path) as Metadata;
  this.vg.server.sockets.to(socketId).emit('log', 'success');

  const met = flatten(
    metadata.streams.map( i => `-map 0:${i.index} -c copy ${dir}/${i.codec_type}${i.index}.${i.codec_name}`.split(' ')),
  );
  await ffmpeg([ path, ...met ], this.vg, socketId);
  this.vg.server.sockets.to(socketId).emit('log', 'success');

  const audioFiles = await readdir(dir);
  this.vg.server.sockets.to(socketId).emit('log', 'success');

  const promises = [];
  audioFiles.forEach( i => {
    if (i.indexOf('video') >= 0 ) return;
    const str = `${dir}/${i} -ar 8000 -ac 1 -acodec libmp3lame ${dir}/${i.split('.').shift()}.${AUDIO_EXT}`.split(' ');
    const res = ffmpeg([ ...flatten(str) ], this.vg, socketId);
    promises.push(res);
  });

  await Promise.all(promises);
  let files = await readdir(dir);
  files = files.filter( f => f.split('.').pop().indexOf(AUDIO_EXT) > -1)
               .map(f => `${SERVER_PATH_TEMP}/${dirId}/${f}`);
  return Promise.resolve({
    path,
    metadata,
    audioFiles: files,
  });
 }

}