import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Hotkeys } from './hotkeys.entity';
import { HotkeysController } from './hotkeys.controller';
import { HotkeysService } from './hotkeys.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Hotkeys]),
  ],
  controllers: [HotkeysController],
  providers: [HotkeysService],
  exports: [HotkeysService],
})
export class HotkeysModule { }