import { Injectable } from '@nestjs/common';
import { Repository, DeleteResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Hotkeys } from './hotkeys.entity';
import { CreateProjectHotkeysDto } from './dto/create-hotkeys.dto';
import { UpdateProjectHotkeysDto } from './dto/update-hotkeys.dto';

@Injectable()
export class HotkeysService {
  constructor(
    @InjectRepository(Hotkeys)
    private readonly hotkeysRepository: Repository<Hotkeys>,
  ) { }

  create(createProjectHotkeysDto: CreateProjectHotkeysDto): Promise<Hotkeys> {
    const hotkeys = { ...new Hotkeys(), ...createProjectHotkeysDto };
    return this.hotkeysRepository.save(hotkeys);
  }

  async fetchAll(): Promise<CreateProjectHotkeysDto[]> {
    return this.hotkeysRepository.find();
  }

  findById(id: string | number): Promise<Hotkeys> {
    return this.hotkeysRepository.findOne(id);
  }

  findByName(name: string): Promise<Hotkeys> {
    return this.hotkeysRepository.findOne({ name });
  }

  async updateById(id: string | number, updateProjectHotkeysDto: UpdateProjectHotkeysDto): Promise<Hotkeys> {
    await this.hotkeysRepository.update(id, updateProjectHotkeysDto);
    return this.findById(id);
  }

  async removeById(id: string | number): Promise<DeleteResult> {
    return this.hotkeysRepository.delete(id);
  }
}