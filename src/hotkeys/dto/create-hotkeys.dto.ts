import { IsNumber, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Project } from '../../project/project.entity';

export class CreateProjectHotkeysDto {
  @IsString()
  @ApiModelProperty()
  readonly name: string;

  @IsString()
  @ApiModelProperty()
  readonly value: string;

  @IsNumber()
  @ApiModelProperty()
  readonly projectId: Project;
}