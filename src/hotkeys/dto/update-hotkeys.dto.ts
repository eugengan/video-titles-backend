import { IsNumber, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { CreateProjectHotkeysDto } from './create-hotkeys.dto';
import { Project } from '../../project/project.entity';

export class UpdateProjectHotkeysDto extends CreateProjectHotkeysDto {
  @IsNumber()
  @ApiModelProperty()
  readonly id: number;

  @IsString()
  @ApiModelProperty()
  readonly name: string;

  @IsString()
  @ApiModelProperty()
  readonly value: string;

  @IsNumber()
  @ApiModelProperty()
  readonly projectId: Project;
}