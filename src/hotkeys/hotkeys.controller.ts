import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  HttpCode,
  UseGuards,
} from '@nestjs/common';
import { Hotkeys } from './hotkeys.entity';
import { HotkeysService } from './hotkeys.service';
import { CreateProjectHotkeysDto } from './dto/create-hotkeys.dto';
import { UpdateProjectHotkeysDto } from './dto/update-hotkeys.dto';
import { DeleteResult } from 'typeorm';
import { ApiOperation, ApiUseTags, ApiResponse, ApiImplicitParam, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

@Controller('hotkeys')
@ApiUseTags('hotkeys')
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class HotkeysController {
  constructor(
    private readonly hotkeysService: HotkeysService,
  ) { }

  @Get()
  @ApiOperation({ title: 'Get all hotkeys' })
  async fetchAll(): Promise<CreateProjectHotkeysDto[]> {
    return this.hotkeysService.fetchAll();
  }

  @Get('/:id')
  @ApiOperation({ title: 'Get hotkeys by id' })
  @ApiImplicitParam({ name: 'id' })
  fetchById(@Param('id') id: number | string): Promise<Hotkeys> {
    return this.hotkeysService.findById(id);
  }

  @Post()
  @ApiOperation({ title: 'Create a new hotkey' })
  async create(@Body() createProjectHotkeysDto: CreateProjectHotkeysDto): Promise<Hotkeys> {
    return this.hotkeysService.create({
      ...createProjectHotkeysDto,
    } as Hotkeys);
  }

  @Put('/:id')
  @ApiOperation({ title: 'Update hotkeys by id' })
  @ApiImplicitParam({ name: 'id' })
  update(
    @Param('id') id: string | number,
    @Body() updateProjectHotkeysDto: UpdateProjectHotkeysDto,
  ): Promise<Hotkeys> {
    return this.hotkeysService.updateById(id, updateProjectHotkeysDto);
  }

  @Delete('/:id')
  @HttpCode(204)
  @ApiOperation({ title: 'Delete hotkeys by is' })
  @ApiImplicitParam({ name: 'id' })
  @ApiResponse({ status: 204, description: 'Successfully deleted hotkeys' })
  remove(
    @Param('id') id: number | string,
  ): Promise<DeleteResult> {
    return this.hotkeysService.removeById(id);
  }
}