import { Entity, ManyToOne, Column } from 'typeorm';
import { BaseEntity } from '../common/entity/base.entity';
import { Project} from '../project/project.entity';

@Entity()
export class Hotkeys extends BaseEntity {
  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false })
  value: string;

  @ManyToOne(type => Project, project => project.hotkeys)
  projectId: Project;
}