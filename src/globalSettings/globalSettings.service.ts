import { Injectable } from '@nestjs/common';
import { Repository, DeleteResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { GlobalSettings } from './globalSettings.entity';
import { CreateGlobalSettingsDto } from './dto/create-globalSettings.dto';
import { UpdateGlobalSettingsDto } from './dto/update-globalSettings.dto';

@Injectable()
export class GlobalSettingsService {
  constructor (
    @InjectRepository(GlobalSettings)
    private readonly globalSettingsRepository: Repository<GlobalSettings>,
  ) { }

  create(createGlobalSettingsDto: CreateGlobalSettingsDto[]): Promise<GlobalSettings[]> {
    const settings = [];
    createGlobalSettingsDto.forEach((item, i) => {
      settings.push({ ...new GlobalSettings(), ...createGlobalSettingsDto[i] })
    })
    return this.globalSettingsRepository.save(settings);
  }

  async fetchAll(): Promise<CreateGlobalSettingsDto[]> {
    return this.globalSettingsRepository.find()
  }

  findById(id: string | number): Promise<GlobalSettings> {
    return this.globalSettingsRepository.findOne(id);
  }

  findByName(name: string): Promise<GlobalSettings> {
    return this.globalSettingsRepository.findOne({ name });
  }

  async updateById(id: string | number, updateGlobalSettingsDto: UpdateGlobalSettingsDto): Promise<GlobalSettings> {
    await this.globalSettingsRepository.update(id, updateGlobalSettingsDto);
    return this.findById(id);
  }

  async removeById(id: number | string): Promise<DeleteResult> {
    return this.globalSettingsRepository.delete(id)
  }
}