import { Entity, Column } from 'typeorm';
import { BaseEntity } from '../common/entity/base.entity';

@Entity()
export class GlobalSettings extends BaseEntity {
  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false })
  value: string;
}