import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  HttpCode,
  UseGuards,
} from '@nestjs/common';
import { GlobalSettings } from './globalSettings.entity';
import { GlobalSettingsService } from './globalSettings.service';
import { CreateGlobalSettingsDto } from './dto/create-globalSettings.dto';
import { UpdateGlobalSettingsDto } from './dto/update-globalSettings.dto';
import { DeleteResult } from 'typeorm';
import { ApiOperation, ApiUseTags, ApiResponse, ApiImplicitParam, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/roles.guard';
import { UserRole } from '../common/enums/user-role.enum';
import { AcceptableRoles } from '../common/decorators/roles.decorator';

@Controller('global-settings')
@ApiUseTags('global-settings')
@UseGuards(AuthGuard('jwt'), RolesGuard)
@ApiBearerAuth()
export class GlobalSettingsController {
  constructor(
    private readonly globalSettingsService: GlobalSettingsService,
  ) { }

  @Get()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Get all globals settings' })
  async fetchAll(): Promise<CreateGlobalSettingsDto[]> {
    return this.globalSettingsService.fetchAll();
  }

  @Get('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Getglobal setting by id' })
  @ApiImplicitParam({ name: 'id' })
  fetchById(@Param('id') id: number | string): Promise<GlobalSettings> {
    return this.globalSettingsService.findById(id);
  }

  @Post()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Create a new global settings' })
  async create(@Body() createGlobalSettingsDto: CreateGlobalSettingsDto[]): Promise<GlobalSettings[]> {
    return this.globalSettingsService.create(createGlobalSettingsDto);
  }

  @Put('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Update global setting by id' })
  @ApiImplicitParam({ name: 'id' })
  update(@Param('id') id: string | number, @Body() updateGlobalSettingsDto: UpdateGlobalSettingsDto): Promise<GlobalSettings> {
    return this.globalSettingsService.updateById(id, updateGlobalSettingsDto);
  }

  @Delete('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @HttpCode(204)
  @ApiOperation({ title: 'Delete global setting by id' })
  @ApiImplicitParam({ name: 'id' })
  @ApiResponse({ status: 204, description: 'Successfuly deleted global setting' })
  remove(@Param('id') id: string | number): Promise<DeleteResult> {
    return this.globalSettingsService.removeById(id);
  }
}