import { IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { CreateGlobalSettingsDto } from './create-globalSettings.dto';

export class UpdateGlobalSettingsDto extends CreateGlobalSettingsDto {
  @IsNumber()
  @ApiModelProperty()
  readonly id: number;
}