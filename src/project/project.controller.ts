import {
  Controller,
  Post,
  Body,
  Put,
  Param,
  Get,
  Delete,
  HttpCode,
  HttpException,
  HttpStatus,
  UseGuards,
  Req,
  Res,
  Patch,
} from '@nestjs/common';
import { ProjectService } from './project.service';
import { CreateProjectDto } from './dto/create-project.dto';
import { Project } from './project.entity';
import { UpdateProjectDto } from './dto/update-project.dto';

import {
  ApiOperation,
  ApiUseTags,
  ApiResponse,
  ApiImplicitParam,
  ApiBearerAuth,
  ApiConsumes,
  ApiImplicitFile,
} from '@nestjs/swagger';
import { SubtitleService } from '../subtitle/subtitle.service';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/roles.guard';
import { UserRole } from '../common/enums/user-role.enum';
import { AcceptableRoles } from '../common/decorators/roles.decorator';
import { DeleteProjectDto } from './dto/delete-project.dto';
import { DeleteResponseDto } from '../common/dto/delete.response.dto';
import { UpdateDashboardProjectDto } from './dto/update-dashboard-project.dto';
import { FileStoreService } from '../common/services/file-store.service';
import { TEMP } from '../common/constants/paths.constants';

@Controller('project')
@ApiUseTags('project')
@UseGuards(AuthGuard('jwt'), RolesGuard)
@ApiBearerAuth()
export class ProjectController {
  constructor(
    private readonly projectService: ProjectService,
    private readonly subtitlesService: SubtitleService,
    private readonly fileStoreService: FileStoreService,
  ) { }

  @Get()
  @AcceptableRoles(UserRole.ADMIN,  UserRole.USER)
  @ApiOperation({ title: 'Get all project' })
  async fetchAll(@Req() req): Promise<Project[]> {
    if (req.user.role === UserRole.USER){
      return this.projectService.fetchByUserId(req.user.id);
    }
    return this.projectService.fetchAll();
  }

  @Get('/:id')
  @ApiOperation({ title: 'Get project by id' })
  @ApiImplicitParam({name: 'id'})
  fetchById(@Param('id') id: number | string): Promise<Project> {
    return this.projectService.findById(id);
  }

  @Post()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Create a new project' })
  async create(@Req() req, @Body() createProjectDto: CreateProjectDto): Promise<Project> {
    const project = await this.projectService.findByName(createProjectDto.name);
    if (project) {
      throw new HttpException(`Project with name ${createProjectDto.name} already exist`, HttpStatus.BAD_REQUEST);
    }

    return this.projectService.create(createProjectDto, req);
  }

  @Put('/:id')
  @AcceptableRoles(UserRole.ADMIN, UserRole.USER)
  @ApiOperation({ title: 'Update project by id' })
  @ApiImplicitParam({name: 'id'})
  async update(
    @Param('id') id: string,
    @Body() updateProjectDto: UpdateProjectDto,
  ): Promise<Project> {
    let { version } = updateProjectDto;
    const foundProject = await this.projectService.findByName(updateProjectDto.name);
    if (!foundProject){
      throw new HttpException(`Project with name ${updateProjectDto.name} don't exist`, HttpStatus.BAD_REQUEST);
    }
    if (foundProject.id !== parseInt(id, 10)) {
      throw new HttpException(`Project with name ${updateProjectDto.name} already exist`, HttpStatus.BAD_REQUEST);
    }
    const project = await this.projectService.updateByIdApp(id, updateProjectDto);
    if (!version){
      return Promise.resolve({...project });
    }

    // TODO: remove in furthers, versions could be saved by titles
    version = await this.subtitlesService.findById(version.id);
    return Promise.resolve({
      ...project,
      version,
    });
  }

  @Patch('/:id/status')
  @AcceptableRoles(UserRole.USER)
  @ApiOperation({ title: 'Update project by id' })
  @ApiImplicitParam({name: 'id'})
  async changeStatus(
    @Param('id') id: string,
    @Body() updateProjectDto: Partial<UpdateProjectDto>,
  ): Promise<Project> {
    const { status } = updateProjectDto;
    const foundProject = await this.projectService.findById(id);
    if (!foundProject){
      throw new HttpException(`Project don't exist`, HttpStatus.BAD_REQUEST);
    }

    return this.projectService.changeStatus(id, status);
  }

  @Put('/dashboard/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Update project by id from dashboard' })
  @ApiImplicitParam({name: 'id'})
  async updateDashboard(
    @Param('id') id: string,
    @Body() updateProjectDto: UpdateDashboardProjectDto,
    @Req() req,
  ): Promise<Project> {

    const foundProject = await this.projectService.findByName(updateProjectDto.name);
    if (!foundProject){
      throw new HttpException(`Project with name ${updateProjectDto.name} don't exist`, HttpStatus.BAD_REQUEST);
    }
    if (foundProject.id !== parseInt(id, 10)) {
      throw new HttpException(`Project with name ${updateProjectDto.name} already exist`, HttpStatus.BAD_REQUEST);
    }

    return this.projectService.updateByIdDashboard(id, updateProjectDto, foundProject, req);
  }

  // TODO: remove in furthers, versions could be saved by titles
  @Post('/autosave')
  @ApiOperation({ title: 'Autosave subtitles by project id' })
  autosaveSubtitles(@Body() autSaveDto: UpdateProjectDto): Promise<Project> {
    return this.projectService.autoSave(autSaveDto);
  }

  @Delete('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiImplicitParam({name: 'id'})
  @HttpCode(204)
  @ApiOperation({ title: 'Delete project by id' })
  @ApiResponse({ status: 204, description: 'Successfully deleted Project' })
  async remove(
    @Param('id') id: number | string,
    @Body() deleteProjectDto: DeleteProjectDto,
    ): Promise<DeleteResponseDto> {
    await this.projectService.removeById(id, deleteProjectDto);
    return { success: true, id: deleteProjectDto.id };
  }

  @Post('/upload-file')
  @ApiOperation({ title: 'Upload file'})
  @ApiResponse({ status: 200, type: String })
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true, description: 'Attached file for project' })
  async uploadSubtitles(@Req() req, @Res() res): Promise<void> {
    if (req.fileValidationError) {
      throw new HttpException(req.fileValidationError, HttpStatus.BAD_REQUEST);
    }
    try{
      const path = await this.fileStoreService.fileUpload(req, res, TEMP);
      res.status(201).send(path);
    }
    catch (err){
      throw new HttpException(`Can\'t upload file: ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}