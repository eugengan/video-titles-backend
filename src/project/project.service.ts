
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Repository, getManager } from 'typeorm';
import { isEmpty } from 'lodash';
import { remove, readdir, readFile } from 'fs-extra';
import { dirname, extname } from 'path';
import * as url from 'url';
import { createReadStream, promises } from 'fs';
import { v4 as uuid } from 'uuid';
import { isEqual, get } from 'lodash';

import { Project } from './project.entity';
import { Hotkeys } from '../hotkeys/hotkeys.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { PROJECTS, TEMP } from '../common/constants/paths.constants';
import { ResultCreateFilesDto } from './dto/resultCreateFiles.dto';
import { Subtitles } from '../subtitle/subtitle.entity';
import { User } from '../user/user.entity';
import { DeleteProjectDto } from './dto/delete-project.dto';
import { UpdateDashboardProjectDto } from './dto/update-dashboard-project.dto';
import { FileStoreService } from '../common/services/file-store.service';
import { FilesProjectDto, FilesProjectByMovingDto } from './dto/files-project.dto';
import { ProjectStatus } from '../common/enums/project-status.enum';
import { CronService } from '../common/services/cron.sevice';
import { EmailService } from '../common/services/email.service';
import { TWO_HOURS } from '../common/constants/common.constants';

const relationsForAll = ['assignedUser', 'createdBy', 'guideline', 'hotkeys', 'titles' ];

const relationsForOne = [...relationsForAll, 'guideline.errors', 'guideline.settings'];

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
    private readonly fileStoreService: FileStoreService,
    private readonly cronService: CronService,
    private readonly emailService: EmailService,
  ) {
    this.cronService.job('0 0 * * *', () => {
      this.cleanNotUsedFiles();
    });
  }

  async cleanNotUsedFiles(): Promise<void> {
    this.fileStoreService.cleanNotUsedFiles(TEMP);
    const rootDir = `${process.env.PWD}/public/assets/${TEMP}`;
    const files = await readdir(rootDir);

    for (const file of files){
      const filePath = `${rootDir}/${file}`;
      const { birthtime } = await promises.stat(filePath);
      if (birthtime.getTime() < Date.now() - TWO_HOURS){
        remove(filePath);
      }
    }
  }
  async create(createProjectDto: CreateProjectDto, req): Promise<Project> {
    let project, version;
    const {
      audioFiles,
      projectDir,
      videoFile,
    } = await this.movingMediaFiles(createProjectDto);
    const { uploadedFiles } = await this.movingAttachedFiles(createProjectDto);

    const [ titles ] = createProjectDto.titles;
    await getManager().transaction(async transactionalEntityManager => {
      project = await transactionalEntityManager.save(Project, {
         ...new Project(),
         ...createProjectDto,
         assignedUser: { id: createProjectDto.assignedUser.id } as User,
         createdBy: { id: createProjectDto.createdBy.id } as User,
         audioFiles,
         projectDir,
         videoFile,
         uploadedFiles,
        });
      const promises = [];

      if (titles && titles.titlesList && titles.titlesList.length ||
          titles && titles.originalTitles && titles.originalTitles.length) {
        version = await transactionalEntityManager.save(
          Subtitles,
          {
            titlesList: titles.titlesList && { ...new Subtitles(), ...titles.titlesList },
            originalTitles: titles.originalTitles && { ...new Subtitles(), ...titles.originalTitles },
            projectId: project.id,
          },
        );
      }

      const projectHotkeys = [];
      for (const hotkey of createProjectDto.hotkeys) {
        const h = {...new Hotkeys(), ...hotkey, projectId: { id: project.id} as Project };
        projectHotkeys.push(h);
      }
      promises.push(transactionalEntityManager.save(Hotkeys, projectHotkeys));
      await Promise.all(promises);
    });
    project = await this.findById(project.id);

    const { assignedUser } = project;
    if (assignedUser) {
      this.emailService.userAssigned(project, {
        email: assignedUser.email,
        name:  assignedUser.name,
        url: `${req.protocol}://${req.headers.host}/dashboard/projects`,
      });
    }

    return Promise.resolve({
      ...project,
      version,
    });
  }

  fetchAll(): Promise<Project[]> {
    return this.projectRepository.find({ relations: relationsForAll });
  }

  fetchByUserId(id: number): Promise<Project[]> {
    return this.projectRepository.find({ assignedUser: { id }});
  }

  findById(id: string | number): Promise<Project> {
    return this.projectRepository.findOne(id, { relations:  relationsForOne });
  }

  findByName(name: string): Promise<Project> {
    return this.projectRepository.findOne({ name }, { relations: relationsForOne });
  }

  async updateByIdApp(id: string | number, updateProjectDto: UpdateProjectDto): Promise<Project> {
    const { version, hotkeys } = updateProjectDto;
    await getManager().transaction(async transactionalEntityManager => {

      const promises = [];
      for (const hotkey of hotkeys) {
        const h = {...new Hotkeys(), ...hotkey, projectId: { id } as Project};
        promises.push(transactionalEntityManager.save(Hotkeys, h));
      }

      promises.push(transactionalEntityManager.save(Subtitles, { ...version, projectId: { id } as Project }));

      await Promise.all(promises);
    });
    return this.findById(id);
  }

  async updateByIdDashboard(
    id: string | number,
    updateProjectDto: UpdateDashboardProjectDto,
    foundProject: Project,
    req,
    ): Promise<Project> {
    const { uploadedFiles } = await this.uploadedFilesByUpdateProject(updateProjectDto, foundProject);
    const { videoFile, audioFiles } = await this.mediaFilesByUpdateProject(updateProjectDto, foundProject);

    const [ titles ] = updateProjectDto.titles;
    await getManager().transaction(async transactionalEntityManager => {
      await transactionalEntityManager.update(Project, id, {
        ...new Project(),
        ...updateProjectDto,
        assignedUser: { id: updateProjectDto.assignedUser.id } as User,
        uploadedFiles,
        videoFile,
        audioFiles,
      } as Project);

      if (foundProject.titlesEdited || updateProjectDto.status !== ProjectStatus.TO_DO){
        return;
      }

      if (titles && titles.titlesList ||
        titles && titles.originalTitles) {
        await transactionalEntityManager.save(Subtitles, {
            ...new Subtitles(),
            ...titles,
            projectId: { id: foundProject.id} as Project,
          },
        );
      }
    });
    const project = await this.findById(id);
    this.sendEmailByUpdateProject(project, foundProject, req);

    return Promise.resolve(project);
  }

  private sendEmailByUpdateProject(project: Project, foundProject: Project, req): void{
    const { assignedUser } = project;
    if (assignedUser && get(assignedUser, 'id') !==  get(foundProject.assignedUser, 'id')) {
      this.emailService.userAssigned(project, {
        email: assignedUser.email,
        name:  assignedUser.name,
        url: `${req.protocol}://${req.headers.host}/dashboard/projects`,
      });
    } else {
      this.emailService.updatedProject(project, {
        email: assignedUser.email,
        name:  assignedUser.name,
        url: `${req.protocol}://${req.headers.host}/dashboard/projects`,
      });
    }
  }
  async changeStatus (id: string | number, status: ProjectStatus): Promise<Project>{
    await this.projectRepository.update(id, { status });
    return this.findById(id);
  }

  async autoSave(updateProjectDto: UpdateProjectDto): Promise<Project> {
    const { version, id, hotkeys } = updateProjectDto;
    const { originalTitles, titlesList } = version;
    await getManager().transaction(async transactionalEntityManager => {

      const promises = [];
      for (const hotkey of hotkeys) {
        const h = {...new Hotkeys(), ...hotkey, projectId: { id } as Project};
        promises.push(transactionalEntityManager.save(Hotkeys, h));
      }

      if (!isEmpty(originalTitles) || !isEmpty(titlesList)) {
        promises.push(transactionalEntityManager.save(Subtitles, {
          ...new Subtitles(),
          originalTitles,
          titlesList,
          projectId: { id } as Project,
        }));
      }
      await Promise.all(promises);
    });
    return this.findById(id);
  }

  async removeById(id: number | string, deleteProjectDto: DeleteProjectDto): Promise<void> {
    const { projectDir } = deleteProjectDto;
    await getManager().transaction(async transactionalEntityManager => {
      await transactionalEntityManager.delete(Hotkeys, { projectId: { id } as Project });
      await transactionalEntityManager.delete(Subtitles, { projectId: { id } as Project});
      if (projectDir) {
        await this.fileStoreService.removeFolder(`${projectDir}/`);
      }
      await transactionalEntityManager.delete(Project, id);
    });
  }

  async movingAttachedFiles(createProjectDto: FilesProjectByMovingDto): Promise<ResultCreateFilesDto>{
    // moving uploaded files in project folder on AWS S3
    const projectDir = `${PROJECTS}/${createProjectDto.name}`;
    if (!createProjectDto.uploadedFiles) {
      return Promise.resolve({ projectDir });
    }
    const hostName = new url.URL(createProjectDto.uploadedFiles[0]).origin;
    const uploadedFiles = [];
    const uF = [];
    const uFd = [];
    createProjectDto.uploadedFiles.forEach(file => {
      const fileName = new url.URL(file).pathname.split('/').pop();
      const filePath = `${projectDir}/${fileName}`;
      const oldFilePath = `${TEMP}/${fileName}`;
      uploadedFiles.push(`${hostName}/${filePath}`);
      uF.push(this.fileStoreService.copyFile(file, filePath));
      uFd.push(this.fileStoreService.removeFile(oldFilePath));
    });
    await Promise.all(uF);
    await Promise.all(uFd);
    return Promise.resolve({ projectDir, uploadedFiles });
  }

  async movingMediaFiles(createProjectDto: FilesProjectByMovingDto): Promise<ResultCreateFilesDto>{
    try{
      // videoFile is exist
      const projectDir = `${PROJECTS}/${createProjectDto.name}`;
      if (!createProjectDto.videoFile) {
        return Promise.resolve({ projectDir, audioFiles: [], videoFile: '' });
      }

      // moving video file in project folder on AWS S3
      const [, filename] = createProjectDto.videoFile.split('__');
      const pathToVideoFile = `${projectDir}/${uuid()}__${filename}`;

      const video = await readFile(createProjectDto.videoFile);
      const contentType = `video/${extname(createProjectDto.videoFile)}`;
      const { Location } = await this.fileStoreService.multipartPartUpload(video, contentType, pathToVideoFile);
      await remove(createProjectDto.videoFile);

      // audioFiles are exist
      if (!createProjectDto.audioFiles) {
        return Promise.resolve({ projectDir, audioFiles: [] });
      }

      // uploading audio files in project folder on AWS S3
      const audioPromises = createProjectDto.audioFiles.map(audio => {
        const audioFileName = audio.split('/').pop();
        const audioPath = `${projectDir}/${uuid()}__${audioFileName}`;
        const readStream = createReadStream(`${process.env.PWD}/public/${audio}`);
        return this.fileStoreService.uploadStream(audioPath, readStream);
      });

      const audioFiles = await Promise.all(audioPromises);

      // removing local temporary audio files
      await Promise.all(
        createProjectDto.audioFiles.map(audio => remove(
          dirname(`${process.env.PWD}/public/${audio}`)),
          ),
      );
      return Promise.resolve({
        audioFiles: audioFiles.map(audio => audio.Location) || [],
        projectDir,
        videoFile: Location,
      });
    }
    catch (err){
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

async uploadedFilesByUpdateProject(
    updateProjectDto: UpdateDashboardProjectDto,
    foundProject: Project,
  ): Promise<FilesProjectDto> {
    const isEqualUploadedFiles = isEqual(
      updateProjectDto.uploadedFiles ? updateProjectDto.uploadedFiles.sort() : updateProjectDto.uploadedFiles,
      foundProject.uploadedFiles ? foundProject.uploadedFiles.sort() : foundProject.uploadedFiles,
    );
    if (isEqualUploadedFiles) {
      return { uploadedFiles: foundProject.uploadedFiles };
    }
    return await this.movingAttachedFiles(updateProjectDto);
  }

  async mediaFilesByUpdateProject(
    updateProjectDto: UpdateDashboardProjectDto,
    foundProject: Project,
  ): Promise<FilesProjectDto> {
    const compareVideoFile = foundProject.videoFile.localeCompare(updateProjectDto.videoFile);
    if (compareVideoFile === 0) {
      return {
        videoFile: foundProject.videoFile,
        audioFiles: foundProject.audioFiles,
      };
    }
    return await this.movingMediaFiles(updateProjectDto);
  }
}