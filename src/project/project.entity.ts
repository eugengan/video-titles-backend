import { Entity, Column, OneToMany, ManyToOne, OneToOne, JoinColumn } from 'typeorm';
import { BaseEntity } from '../common/entity/base.entity';
import { Metadata } from '../video/dto/metadata.dto';
import { Hotkeys } from '../hotkeys/hotkeys.entity';
import { Subtitles } from '../subtitle/subtitle.entity';
import { GuideLines } from '../guidelines/guidelines.entity';
import { ProjectStatus } from '../common/enums/project-status.enum';
import { User } from '../user/user.entity';

@Entity()
export class Project extends BaseEntity{

  @Column({ nullable: false, unique: true, type: 'varchar' })
  name: string;

  @Column({ nullable: false, default: '' })
  videoFile: string;

  @Column({ type: 'simple-array', default: null })
  audioFiles: Array<string>;

  @Column({ type: 'json', default: null })
  metadata: Metadata;

  @Column()
  projectDir: string;

  @OneToMany(type => Subtitles, titles => titles.projectId)
  titles: Subtitles[];

  @Column({ default: false })
  titlesEdited: boolean;

  @OneToOne(type => GuideLines)
  @JoinColumn()
  guideline: GuideLines;

  @OneToMany(type => Hotkeys, hotkeys => hotkeys.projectId)
  hotkeys: Hotkeys[];

  @Column({ type: 'enum', enum: ProjectStatus, default: ProjectStatus.TO_DO })
  status: ProjectStatus;

  @ManyToOne(type => User, user => user.assignedProject)
  assignedUser: User;

  @ManyToOne(type => User, user => user.createdProject)
  createdBy: User;

  @Column({ type: 'simple-array', default: null })
  uploadedFiles: Array<string>;
}
