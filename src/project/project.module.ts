import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Project } from './project.entity';
import { ProjectController } from './project.controller';
import { ProjectService } from './project.service';
import { SubtitleModule } from '../subtitle/subtitle.module';
import { FileStoreService } from '../common/services/file-store.service';
import { CronService } from '../common/services/cron.sevice';
import { EmailService } from '../common/services/email.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Project]),
    SubtitleModule,
  ],
  controllers: [ProjectController],
  providers: [
    ProjectService,
    FileStoreService,
    CronService,
    EmailService,
  ],
  exports: [ProjectService],
})
export class ProjectModule { }
