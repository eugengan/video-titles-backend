import { IsString, IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class DeleteProjectDto {
  @IsNumber()
  @ApiModelProperty()
  readonly id: number;

  @IsString()
  @ApiModelProperty({ description: 'path to video'})
  readonly videoFile: string;

  @IsString()
  @ApiModelProperty()
  readonly projectDir: string;
}