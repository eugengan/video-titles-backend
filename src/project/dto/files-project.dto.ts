import { IsString, IsOptional, IsArray } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class FilesProjectDto {
  @IsOptional() @IsArray() @ApiModelProperty() readonly audioFiles?: Array<string>;
  @IsOptional() @IsArray() @ApiModelProperty() readonly uploadedFiles?: Array<string>;
  @IsOptional() @IsString() @ApiModelProperty() readonly videoFile?: string;
}

export class FilesProjectByMovingDto extends FilesProjectDto{
  @IsString() @ApiModelProperty() readonly name: string;
}