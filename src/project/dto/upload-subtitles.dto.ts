import { IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UploadSubtitlesDto {
  @IsString() @ApiModelProperty() readonly id: string;
  @IsString() @ApiModelProperty() readonly translation: string;
}
