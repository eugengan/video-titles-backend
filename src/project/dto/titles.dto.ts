import { IsString, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class SubtitlesDto {
  @IsOptional()
  @IsString()
  @ApiModelProperty({ description: 'subtitles number'})
  readonly N?: string;

  @IsOptional()
  @IsString()
  @ApiModelProperty({ description: 'text block raised'})
  readonly R?: string;

  @IsOptional()
  @IsString()
  @ApiModelProperty({ description: 'text alignment'})
  readonly A?: string;

  @IsOptional()
  @IsString()
  @ApiModelProperty({ description: 'text color'})
  readonly C?: string;

  @IsOptional()
  @IsString()
  @ApiModelProperty({ description: 'text format'})
  readonly F?: string;

  @IsOptional()
  @IsString()
  @ApiModelProperty({ description: 'text effects'})
  readonly E?: string;

  @IsString()
  @ApiModelProperty({ description: 'timecode in'})
  readonly I: string;

  @IsString()
  @ApiModelProperty({ description: 'timecode out'})
  readonly O: string;

  @IsOptional()
  @IsString()
  @ApiModelProperty({ description: 'subtitles text'})
  readonly T?: string;

  @IsOptional()
  @IsString()
  @ApiModelProperty({ description: 'subtitles text in right block'})
  readonly customT?: string;
}
