import { IsString, IsArray, ValidateNested, IsOptional, IsEnum } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Metadata } from '../../video/dto/metadata.dto';
import { CreateProjectHotkeysDto } from '../../hotkeys/dto/create-hotkeys.dto';
import { Type } from 'class-transformer';
import { CreateSubtitleWithProjectDto } from '../../subtitle/dto/create-subtitle-with-project.dto';
import { UpdateGuideLinesDto } from '../../guidelines/dto/update-guidelines.dto';
import { ProjectStatus } from '../../common/enums/project-status.enum';
import { UpdateUserDto } from '../../user/dto/update-user.dto';

export class CreateProjectDto {
  @IsString()
  @ApiModelProperty()
  readonly name: string;

  @IsOptional()
  @IsString()
  @ApiModelProperty({ description: 'path to video'})
  readonly videoFile?: string;

  @IsOptional()
  @IsArray()
  @ApiModelProperty()
  readonly audioFiles?: Array<string>;

  @IsOptional()
  @ValidateNested()
  @ApiModelProperty()
  readonly metadata?: Metadata;

  @ValidateNested()
  @ApiModelProperty({ type: UpdateGuideLinesDto })
  readonly guideline: UpdateGuideLinesDto;

  @IsOptional()
  @ValidateNested()
  @ApiModelProperty()
  readonly hotkeys?: CreateProjectHotkeysDto[];

  @IsOptional()
  @IsArray()
  @ValidateNested()
  @Type(() => CreateSubtitleWithProjectDto)
  @ApiModelProperty({ type: CreateSubtitleWithProjectDto, isArray: true })
  readonly titles?: CreateSubtitleWithProjectDto[];

  @IsOptional()
  @IsEnum(ProjectStatus)
  @ApiModelProperty({ enum: ProjectStatus })
  readonly status?: ProjectStatus;

  @IsOptional()
  @ValidateNested()
  @ApiModelProperty({ type: UpdateUserDto })
  readonly assignedUser?: UpdateUserDto;

  @ValidateNested()
  @ApiModelProperty({ type: UpdateUserDto })
  readonly createdBy: UpdateUserDto;

  @IsOptional()
  @IsArray()
  @ApiModelProperty({ type: Array })
  readonly uploadedFiles?: Array<string>;
}
