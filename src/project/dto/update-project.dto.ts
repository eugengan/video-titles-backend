import { IsString, IsOptional, IsNumber, IsArray, ValidateNested, IsEnum } from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { Metadata } from '../../video/dto/metadata.dto';
import { UpdateProjectHotkeysDto } from '../../hotkeys/dto/update-hotkeys.dto';
import { UpdateSubtitleDto } from '../../subtitle/dto/update-subtitle.dto';
import { Type } from 'class-transformer';
import { UpdateGuideLinesDto } from '../../guidelines/dto/update-guidelines.dto';
import { ProjectStatus } from '../../common/enums/project-status.enum';
import { UpdateUserDto } from '../../user/dto/update-user.dto';

export class UpdateProjectDto {
  @IsNumber()
  @ApiModelPropertyOptional()
  readonly id: number;

  @IsString()
  @ApiModelProperty()
  readonly name: string;

  @IsOptional()
  @IsString()
  @ApiModelProperty({ description: 'path to video'})
  readonly videoFile?: string;

  @IsOptional()
  @IsArray()
  @ApiModelProperty()
  readonly audioFiles?: Array<string>;

  @IsOptional()
  @ValidateNested()
  @ApiModelProperty()
  readonly metadata?: Metadata;

  @IsString()
  @ApiModelProperty()
  readonly projectDir: string;

  @ValidateNested()
  @ApiModelProperty({ type: UpdateGuideLinesDto })
  readonly guideline: UpdateGuideLinesDto;

  @IsOptional()
  @ValidateNested()
  @ApiModelProperty()
  readonly hotkeys?: UpdateProjectHotkeysDto[];

  @ValidateNested()
  @ApiModelProperty()
  readonly version?: UpdateSubtitleDto;

  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateSubtitleDto)
  @ApiModelProperty({ type: UpdateSubtitleDto, isArray: true })
  readonly titles?: UpdateSubtitleDto[];

  @IsOptional()
  @IsEnum(ProjectStatus)
  @ApiModelProperty({ enum: ProjectStatus })
  readonly status?: ProjectStatus;

  @IsOptional()
  @ValidateNested()
  @ApiModelProperty({ type: UpdateUserDto })
  readonly assignedUser?: UpdateUserDto;

  @IsOptional()
  @IsArray()
  @ApiModelProperty({ type: Array })
  readonly uploadedFiles?: Array<string>;
}