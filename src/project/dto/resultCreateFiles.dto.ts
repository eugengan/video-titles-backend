import { IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { FilesProjectDto } from './files-project.dto';

export class ResultCreateFilesDto extends FilesProjectDto {
  @IsString() @ApiModelProperty() readonly projectDir: string;
}