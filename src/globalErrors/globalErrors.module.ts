import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GlobalErrors } from './globalErrors.entity';
import { GlobalErrorsController } from './globalErrors.controller';
import { GlobalErrorsService } from './globalErrors.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([GlobalErrors]),
  ],
  controllers: [GlobalErrorsController],
  providers: [GlobalErrorsService],
  exports: [GlobalErrorsService],
})
export class GlobalErrorsModule { }
