import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  HttpCode,
  UseGuards,
} from '@nestjs/common';
import { GlobalErrors } from './globalErrors.entity';
import { GlobalErrorsService } from './globalErrors.service';
import { CreateGlobalErrorsDto } from './dto/create-globalErrors.dto';
import { UpdateGlobalErrorsDto } from './dto/update-globalErrors.dto';
import { DeleteResult } from 'typeorm';
import { ApiOperation, ApiUseTags, ApiResponse, ApiImplicitParam, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/roles.guard';
import { UserRole } from '../common/enums/user-role.enum';
import { AcceptableRoles } from '../common/decorators/roles.decorator';

@Controller('global-errors')
@ApiUseTags('global-errors')
@UseGuards(AuthGuard('jwt'), RolesGuard)
@ApiBearerAuth()
export class GlobalErrorsController {
  constructor(
    private readonly errorsService: GlobalErrorsService,
  ) { }

  @Get()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Get all errors' })
  async fetchAll(): Promise<CreateGlobalErrorsDto[]> {
    return this.errorsService.fetchAll();
  }

  @Get('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Get error by id' })
  @ApiImplicitParam({ name: 'id' })
  fetchById(@Param('id') id: number | string): Promise<GlobalErrors> {
    return this.errorsService.findById(id);
  }

  @Post()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Create a new error' })
  async create(@Body() CreateGlobalErrorsDto: CreateGlobalErrorsDto[]): Promise<GlobalErrors[]> {
    return this.errorsService.create(CreateGlobalErrorsDto);
  }

  @Put('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Update error by id' })
  @ApiImplicitParam({ name: 'id' })
  update(@Param('id') id: string | number, @Body() updateErrorsDto: UpdateGlobalErrorsDto): Promise<GlobalErrors> {
    return this.errorsService.updateById(id, updateErrorsDto);
  }

  @Delete('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @HttpCode(204)
  @ApiOperation({ title: 'Delete error by id' })
  @ApiImplicitParam({ name: 'id' })
  @ApiResponse({ status: 204, description: 'Successfully deleted error' })
  remove(@Param('id') id: string | number): Promise<DeleteResult> {
    return this.errorsService.removeById(id);
  }
}