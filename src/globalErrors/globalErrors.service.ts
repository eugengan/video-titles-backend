import { Injectable } from '@nestjs/common';
import { Repository, DeleteResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { GlobalErrors } from './globalErrors.entity';
import { CreateGlobalErrorsDto } from './dto/create-globalErrors.dto';
import { UpdateGlobalErrorsDto } from './dto/update-globalErrors.dto';

@Injectable()
export class GlobalErrorsService {
  constructor(
    @InjectRepository(GlobalErrors)
    private readonly errorsRepository: Repository<GlobalErrors>,
  ) { }

  create(createErrorsDto: CreateGlobalErrorsDto[]): Promise<GlobalErrors[]> {
    const error = createErrorsDto.map(ce => ({ ...new GlobalErrors(), ...ce }));
    return this.errorsRepository.save(error);
  }

  async fetchAll(): Promise<CreateGlobalErrorsDto[]> {
    return this.errorsRepository.find();
  }

  findById(id: string | number): Promise<GlobalErrors> {
    return this.errorsRepository.findOne(id, { relations: ['guideline']});
  }

  findByName(name: string): Promise<GlobalErrors> {
    return this.errorsRepository.findOne({ name });
  }

  async updateById(id: string | number, updateErrorsDto: UpdateGlobalErrorsDto): Promise<GlobalErrors> {
    await this.errorsRepository.update(id, updateErrorsDto);
    return this.findById(id);
  }

  async removeById(id: number | string): Promise<DeleteResult> {
    return this.errorsRepository.delete(id);
  }
}