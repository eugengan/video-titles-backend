import { IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { CreateGlobalErrorsDto } from './create-globalErrors.dto';

export class UpdateGlobalErrorsDto extends CreateGlobalErrorsDto {
  @IsNumber()
  @ApiModelProperty()
  readonly id: number;
}