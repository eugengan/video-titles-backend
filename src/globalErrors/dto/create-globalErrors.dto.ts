import { IsString, IsOptional, IsEnum } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { ERROR_PERMISSIONS } from '../../common/enums/error.permissions.enum';
import { ERROR_COLORS } from '../../common/enums/error.colors.enum';

export class CreateGlobalErrorsDto {
  @IsString()
  @ApiModelProperty()
  readonly name: string;

  @IsString()
  @ApiModelProperty()
  readonly value: string;

  @IsEnum(ERROR_PERMISSIONS)
  @IsOptional()
  @ApiModelProperty({ enum: ERROR_PERMISSIONS })
  readonly permissions?: ERROR_PERMISSIONS;

  @IsEnum(ERROR_COLORS)
  @IsOptional()
  @ApiModelProperty({ enum: ERROR_COLORS })
  readonly color?: ERROR_COLORS;
}