import {
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import * as youtubedl from 'youtube-dl';
import { createWriteStream } from 'fs';
import { Validator } from 'class-validator';
import { isString } from 'lodash';
import { v4 as uuid } from 'uuid';

import { ExtType } from '../enums/extType.enum';
import { REG_YOUTUBE_URL, PATH_TEMP } from '../constants/paths.constants';
import { YoutbeMetadataDto } from '../../video/dto/youtube.dto';
import parseUploadVideoErrors from '../utils/parseUploadVideoError';

export function YoutubeDownload() {
  return (target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => {
      const originalMethod = descriptor.value;

      descriptor.value = async function(...args) {
        const requestBody = args[0];
        const url = requestBody.path;

        const pattern = new RegExp(REG_YOUTUBE_URL, 'i');
        if (!pattern.test(url)) {
          return originalMethod.apply(this, args);
        }

        const video = youtubedl(url, ['--format=18'],  { cwd: __dirname });

        const getVideoName = new Promise((resolve, reject) => {
          video.on('info', (info: YoutbeMetadataDto) => {
            const validator = new Validator();

            if (!isString(info.ext) || !validator.isEnum(info.ext, ExtType)) {
              throw new HttpException('Unsupported mime type', HttpStatus.BAD_REQUEST);
            }

            resolve(info._filename);
          });
          video.on('error', (error: any) => {
            reject(error);
          });
        });
        try {
          const fileName = await getVideoName;
          const filePath = `${PATH_TEMP}/${uuid()}__${fileName}`;

          const writeStream = createWriteStream(filePath);

          video.pipe(writeStream);

          const downloadComplete = new Promise((resolve) => {
            writeStream.on('finish', () => {
              resolve();
            });
          });

          await downloadComplete;
          requestBody.downloadedVideoPath = filePath;

          return originalMethod.apply(this, args);
          } catch (err) {
            throw new HttpException(parseUploadVideoErrors(err.message), HttpStatus.BAD_REQUEST);
        }
      };
      return descriptor;
  };
}
