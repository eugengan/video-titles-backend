import { clone } from 'lodash';
import { SubtitlesDto } from '../../project/dto/titles.dto';

const CT = 'CT:';
const TG = 'TG:'
const END = 'END';

function getAllText(dd: Array<string>, i: number, nextValue: string): string {
	const arrTitle = clone(dd);
	let nextIndex = 0;
	if (nextValue === CT || nextValue === TG) {
		nextIndex = arrTitle.findIndex(item => item.search(nextValue) >= 0);
		nextIndex = nextIndex > -1 ?  nextIndex : dd.length;
	}
	if (nextValue === END) {
		nextIndex = dd.length;
	}
	const sliced = arrTitle.slice( i, nextIndex);
	let text = '';
	sliced.forEach(e => {
		const [ first, second ] = e.split(':');
		text = text ? `${text}\n${first}` : second;
	});
	return text;
}

function parseSubtitlesData(data: string): SubtitlesDto[] {
  const subtitles = [];
  const parsedData = data.split('\n\n');

  parsedData.forEach(d => {
    let title = {};
    const dd = d.split('\n');
    dd.forEach((item, i) => {
      const [key, , m, s, f] = item.split(':');
      let [, value ] = item.split(':');
      if (!key || !value ) return;
      switch (key) {
        case 'T':
          value = getAllText(dd, i, CT);
          break;
        case 'CT':
          value = getAllText(dd, i, END);
          break;
        case 'I':
        case 'O':
          title = { ...title, [key]: `${value}:${m}:${s}:${f}` };
          return;
      }
      if (key) title = { ...title, [key]: value };
    });
    if (Object.keys(title).length){
      subtitles.push(title);
    }
  });

  return subtitles;
}

export default parseSubtitlesData;
