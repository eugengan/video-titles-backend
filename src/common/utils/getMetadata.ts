import * as ffprobe from 'ffprobe';
import { path as ffprobeStatic } from 'ffprobe-static';

export const getMetadata = path =>
  new Promise((resolve, reject) => {
    ffprobe(path, { path: ffprobeStatic }, (err, metadata) => {
      if (err) {
        console.log(err);
        reject(err);
        return;
      }
      console.log('metadata', metadata);
      resolve(metadata);
    });
  });
