
import { spawn } from 'child_process';
import {path as ffmpegpath } from 'ffmpeg-static';
import { VideoGateway } from '../../video/video.gateway';

export const ffmpegStatic = (met, vg: VideoGateway, socketId: number | string) =>
  new Promise((resolve, reject) => {
    const ffmpeg = spawn( ffmpegpath, ['-i', ...met]);
    ffmpeg.stderr.on( 'data', data => {
      console.log( `stderr: ${data}` );
    });
    ffmpeg.on('close', (code) => {
      resolve();
    });
  });
