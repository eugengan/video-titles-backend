export default function(error: string): string {
  const checkWord = 'ERROR:'
  if (error.includes(checkWord)) {
    const subStr = error.substr(error.indexOf(checkWord) + checkWord.length + 1);
    return subStr;
  }
  return error;
}