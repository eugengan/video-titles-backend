import { IsString, IsOptional, IsEmail, IsUrl } from 'class-validator';

export class CustomEmailOptionsDto {
  @IsString()
  readonly to: string;

  @IsString()
  readonly subject: string;

  @IsString()
  readonly text: string;

  @IsString()
  readonly template: string;

  @IsOptional()
  readonly context?: Object;
}

export class UserCreationEmailOptionsDto {
  @IsEmail()
  readonly email: string;

  @IsString()
  readonly password: string;

  @IsString()
  readonly name: string;

  @IsUrl()
  readonly url: string;
}