import * as dictonary from 'dictionary-en-us';
import * as nspell from 'nspell';
import { SubtitlesDto } from '../../project/dto/titles.dto';
import { SpellCheckResultDto } from '../../subtitle/dto/spell-check-result.dto';

let spell = null;
dictonary(ondictonary);

function ondictonary(err, dict) {
  if (err) {
    throw err;
  }
  spell = nspell(dict);
}

export default (title: SubtitlesDto): SpellCheckResultDto => {
  let failWords = [];
  if (!title || title && !title.T) return {
    ...title,
    failWords,
  };
  const numbersPattern = new RegExp('^[0-9]+$', 'i');
  failWords = title.T.replace( /\n/g, ' ').split(' ')
                    .filter(word => {
                      if (numbersPattern.test(word)) return;
                      if (!spell.correct(word)) {
                        return word;
                      }
                    });
  return {
    ...title,
    failWords,
  };
};