
import { createWriteStream } from 'fs';
import { SubtitlesDto } from '../../project/dto/titles.dto';
import { TRANSLATION_FILE_SUBTITLES } from '../constants/subtitle.constanst';
import { pick } from 'lodash';
import { PATH_TEMP, SERVER_PATH_TEMP } from '../constants/paths.constants';

export default (titles: Array<SubtitlesDto>, projectDir: string, host: string): string => {
	const pathToSave = `${PATH_TEMP}/${TRANSLATION_FILE_SUBTITLES}`;
	const wstream = createWriteStream(pathToSave);
	titles.forEach(title => {
		if (!Object.keys(title).length){
			return;
		}
		const t = pick(title, ['N', 'R', 'A', 'C', 'F', 'E', 'I', 'O', 'T', 'CA', 'B', 'TG']);
		wstream.write(`N:${t.N}\n`);

		Object.entries(t).forEach(([key, value]) => {
				if (key === 'N') {
					return;
				}
				wstream.write(`${key}:${value}\n`);
		});

		wstream.write('\n');
	});
	wstream.end();
	return `${host}${SERVER_PATH_TEMP}/${TRANSLATION_FILE_SUBTITLES}`;
};
