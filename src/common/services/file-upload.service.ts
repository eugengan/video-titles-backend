import { diskStorage } from 'multer';
import { v4 as uuid } from 'uuid';

import { PATH_UPLOAD } from '../constants/paths.constants';

const s = diskStorage({
  destination: PATH_UPLOAD,
  filename(req, file, cb) {
    cb(null, `${uuid()}__${file.originalname}`);
  },
});

export const multerOptions = {
  storage: s,
  limits: {
    files: 1,
    fileSize: 1024 * 1024 * 5,
  },
};