import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { MailerService } from '@nest-modules/mailer';
import { CustomEmailOptionsDto, UserCreationEmailOptionsDto } from 'common/dto/email.dto';
import { Project } from '../../project/project.entity';

@Injectable()
export class EmailService {
  constructor(private readonly mailerService: MailerService) {}

  public sendingCustomEmail(data: CustomEmailOptionsDto): void {
    try {
      this.mailerService.sendMail({
        ...data,
        from: process.env.EMAIL_USER,
      });
    }
    catch (err) {
      throw new HttpException(`Error: ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public userCreation(data: UserCreationEmailOptionsDto): void {
    const { email, password, name, url} =  data;
    try {
      this.mailerService.sendMail({
        from: process.env.EMAIL_USER,
        to: email,
        subject: 'Creation account on Screens Digital Films ✔',
        template: 'welcome-user-email-template',
        context: {
          email,
          password,
          name,
          url,
        },
      });
    }
    catch (err) {
      throw new HttpException(`Error: ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public userAssigned(project: Partial<Project>, user: Partial<UserCreationEmailOptionsDto>): void {
    const { email, name, url } =  user;
    try {
      this.mailerService.sendMail({
        from: process.env.EMAIL_USER,
        to: email,
        subject: `You assigned to project ${project.name} on Screens Digital Films ✔`,
        template: 'assign-user-email-template',
        context: {
          projectName: project.name,
          name,
          url,
        },
      });
    }
    catch (err) {
      throw new HttpException(`Error: ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public updatedProject(project: Partial<Project>, user: Partial<UserCreationEmailOptionsDto>): void {
    const { email, name, url } =  user;
    try {
      this.mailerService.sendMail({
        from: process.env.EMAIL_USER,
        to: email,
        subject: `Project ${project.name} on Screens Digital Films was changed`,
        template: 'update-project-email-template',
        context: {
          projectName: project.name,
          name,
          url,
        },
      });
    }
    catch (err) {
      throw new HttpException(`Error: ${err.message}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}