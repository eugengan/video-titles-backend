
import { readFile } from 'fs-extra';
import { SubtitlesDto } from '../../project/dto/titles.dto';
import parseSubtitlesData from '../utils/parseSubtitlesData';

export default async (path: string): Promise<SubtitlesDto[]> => {
	if (!path) return Promise.resolve([]);

	const rawData = await readFile(path);
	const subtitles = parseSubtitlesData(rawData.toString());

	return Promise.resolve(subtitles);
};
