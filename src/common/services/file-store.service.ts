import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import * as multer from 'multer';
import * as multerS3 from 'multer-s3';
import { v4 as uuid } from 'uuid';
import { Validator } from 'class-validator';
import { S3 } from 'aws-sdk';

import s3 from './aws.service';
import { MimeType } from '../enums/mimeType.enum';
import { TWO_HOURS } from '../../common/constants/common.constants';

const { AWS_S3_BUCKET_NAME } = process.env;
const ACL = 'public-read';
@Injectable()
export class FileStoreService {
  constructor(){}

  private upload = (folder: string) => multer({
    storage: multerS3({
      s3,
      bucket: AWS_S3_BUCKET_NAME,
      acl: ACL,
      key: (req, file, cb: Function) => {
        cb(null, `${folder}/${uuid()}__${file.originalname}`);
      },
    }),
    fileFilter: this.fileFilter,
  }).array('file', 1)

  private fileFilter(req, file, cb: Function) {
    const validator = new Validator();
    if (req.isFiltering && !validator.isEnum(file.mimetype, MimeType)) {
        req.fileValidationError = 'unsupported mime type';
        cb(null, false);
    } else {
        cb(null, true);
    }
  }

  fileUpload(req, res, folder: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.upload(folder)(req, res, err => {
        if (err){
          reject(err);
          return;
        }
        resolve(req.files[0].location);
      });
    });
  }

  uploadStream(path: string, body): Promise<S3.ManagedUpload.SendData> {
    return s3.upload({
      Bucket: AWS_S3_BUCKET_NAME,
      ACL,
      Key: path,
      Body: body,
    }).promise();
  }

  completeMultipartUpload(doneParams): Promise<S3.Types.CompleteMultipartUploadOutput> {
    return s3.completeMultipartUpload({
      ...doneParams,
      Bucket: AWS_S3_BUCKET_NAME,
    }).promise();
  }

  createMultipartUpload(params): Promise<S3.Types.CreateMultipartUploadOutput> {
    return s3.createMultipartUpload({ ...params }).promise();
  }

  uploadPart(partParams): Promise<S3.Types.UploadPartOutput> {
    return s3.uploadPart({
      ...partParams,
      Bucket: AWS_S3_BUCKET_NAME,
    }).promise();
  }

  async multipartPartUpload(file: Buffer, contentType: string, path: string){
    try{
      const partSize = 1024 * 1024 * 50;
      let partNum = 0;
      const multipartMap = {
        Parts: [],
      };
      const multipart = await this.createMultipartUpload({
        Bucket: AWS_S3_BUCKET_NAME,
        ACL,
        Key: path,
        ContentType: contentType,
      });
      for (let rangeStart = 0; rangeStart < file.length; rangeStart += partSize) {
        partNum++;
        const end = Math.min(rangeStart + partSize, file.length);
        const partParams = {
              Body: file.slice(rangeStart, end),
              Key: path,
              PartNumber: partNum,
              UploadId: multipart.UploadId,
            };

        // Send a single part
        console.log('Uploading part: #', partParams.PartNumber, ', Range start:', rangeStart);
        const mData = await this.uploadPart(partParams);
        multipartMap.Parts[ partNum - 1 ] = {
          ETag: mData.ETag,
          PartNumber: Number(partNum),
        };
      }
      return await this.completeMultipartUpload({
        MultipartUpload: multipartMap,
        UploadId: multipart.UploadId,
        Key: path,
      });
    }
    catch (err){
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  copyFile(CopySource: string, Key: string): Promise<S3.Types.CopyObjectOutput> {
    return s3.copyObject({
      Bucket: AWS_S3_BUCKET_NAME,
      ACL,
      CopySource,
      Key,
    }).promise();
  }

  removeFile(Key: string): Promise<S3.Types.DeleteObjectOutput> {
    return s3.deleteObject({
      Bucket: AWS_S3_BUCKET_NAME,
      Key,
    }).promise();
  }

  async removeFolder(Prefix: string): Promise<void> {
    const listedObjects = await this.getListObjects(Prefix);
    if (!listedObjects.Contents.length) {
      return;
    }

    const deleteParams = {
      Bucket: AWS_S3_BUCKET_NAME,
      Delete: { Objects: [] },
    };
    deleteParams.Delete.Objects = listedObjects.Contents.map(({ Key }) => ({ Key }));

    await s3.deleteObjects(deleteParams).promise();
  }

  async cleanNotUsedFiles(Prefix: string): Promise<void> {

    const listedObjects = await this.getListObjects(Prefix);
    if (!listedObjects.Contents.length) {
      return;
    }

    const deleteParams = {
      Bucket: AWS_S3_BUCKET_NAME,
      Delete: { Objects: [] },
    };
    deleteParams.Delete.Objects = listedObjects.Contents.reduce((acc, { Key, LastModified }) => {
      if (LastModified.getTime() < Date.now() - TWO_HOURS){
        acc.push({ Key });
      }
      return acc;
    }, []);

    await s3.deleteObjects(deleteParams).promise();

  }

  async getListObjects(Prefix: string): Promise<S3.Types.ListObjectsV2Output> {
    const listParams = {
      Bucket: AWS_S3_BUCKET_NAME,
      Prefix,
    };
    return await s3.listObjectsV2(listParams).promise();
  }
}