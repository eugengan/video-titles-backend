import { Injectable } from '@nestjs/common';
import * as cron from 'node-cron';

@Injectable()
export class CronService {
  constructor(){}

  job(mask: string, cb: Function): void {
    if (!mask || !cb) return;
    cron.schedule(mask, () => {
      cb();
    });
  }
}