import { Validator } from 'class-validator';
import { diskStorage } from 'multer';
import { v4 as uuid } from 'uuid';

import { MimeType } from '../enums/mimeType.enum';
import { PATH_TEMP } from '../constants/paths.constants';

const s = diskStorage({
  destination: PATH_TEMP,
  filename(req, file, cb) {
    cb(null, `${uuid()}__${file.originalname}`);
  },
});

const ff = function fileFilter(req, file, cb) {
  const validator = new Validator();
  if (!validator.isEnum(file.mimetype, MimeType)) {
      req.fileValidationError = 'unsupported mime type';
      cb(null, false);
  } else {
      cb(null, true);
  }
};
export const multerOptions = {
  storage: s,
  fileFilter: ff,
};