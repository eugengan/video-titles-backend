import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import * as youtubedl from 'youtube-dl';
import { Validator } from 'class-validator';
import { isString } from 'lodash';
import { v4 as uuid } from 'uuid';

import { YoutbeMetadataDto } from '../../video/dto/youtube.dto';
import { ExtType } from '../enums/extType.enum';
import { TEMP } from '../constants/paths.constants';

@Injectable()
export class YoutubeUploadService {
  constructor(){}
  private getName (video){
    return new Promise((resolve, reject) => {
      video.on('info', (info: YoutbeMetadataDto) => {
        const validator = new Validator();
  
        if (!isString(info.ext) || !validator.isEnum(info.ext, ExtType)) {
          throw new HttpException('Unsupported mime type', HttpStatus.BAD_REQUEST);
        }
  
        resolve(info._filename);
      });
      video.on('error', (error: any) => {
        reject(error)
      })
    });
  }   

  async getVideoName(url: string, uploadStream: Function): Promise<string>{    
    try {
      const video = youtubedl(url, ['--format=18'],  { cwd: __dirname });     
      
      const fileName = await this.getName(video);
      const pathToFile = `${TEMP}/${uuid()}__${fileName}`;
      const data = await uploadStream(pathToFile, video);
      return Promise.resolve(data.Location);
    }
    catch (err) {
      console.log(err)
      throw new HttpException(`Can't upload video file from url. ${err}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


}