export enum ExtType {
  MP4 = 'mp4',
  MOV = 'mov',
  FLV = 'flv',
  AVI = 'avi',
  WMV = 'wmv',
}
