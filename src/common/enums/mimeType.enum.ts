export enum MimeType {
  MP4 = 'video/mp4',
  MOV = 'video/mov',
  FLV = 'video/flv',
  AVI = 'video/avi',
  WMV = 'video/wmv',
}
