export enum UserStatus {
  ACTIVE = 'ACTIVE',
  SUSPEND = 'SUSPEND',
}
