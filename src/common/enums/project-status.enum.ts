export enum ProjectStatus {
  TO_DO = 'To Do',
  IN_PROGRESS = 'In Progress',
  NEED_REVIEW = 'Need review',
  COMPLETE = 'Complete',
}