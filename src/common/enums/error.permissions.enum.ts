export enum ERROR_PERMISSIONS {
  DO_NOT_CHECK = 'Don\'t check',
  ALLOW_SUBMIT = 'Allow submit but warn first',
  WARN_NOT_SUBMIT = 'Warn and not allow to submit',
}