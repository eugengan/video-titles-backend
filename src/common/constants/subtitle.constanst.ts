export const REMOTE_FILE = 'REMOTE_FILE';
export const LOCAL_FILE = 'LOCAL_FILE';
export const AUDIO_EXT = 'mp3';
export const FILE_DATA = 'data.sdt';
export const ORIGINAL_FILE_SUBTITLES = 'original-subtitles.pss';
export const TRANSLATION_FILE_SUBTITLES = 'translation-subtitles.pss';
export const TEMP_DATA_FOLDER = 'tempdata';
export const VERSION_LIMIT = 30;