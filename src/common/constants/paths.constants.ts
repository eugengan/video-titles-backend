export const PATH_UPLOAD =  process.env.PWD + '/public/assets/upload';
export const PATH_TEMP =  process.env.PWD + '/public/assets/temp';
export const PATH_PROJECTS =  process.env.PWD + '/public/assets/projects';

export const REG_URL_PATTERN = '((http|https)(:\/\/))?([a-zA-Z0-9]){2}[a-zA-z0-9]+(\/{1}[a-zA-Z0-9]+)*\/?.*(\.mp4|\.mov|\.flv|\.avi|\.wmv)';
export const REG_YOUTUBE_URL = '^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+';

export const SERVER_PATH_UPLOAD =  '/assets/upload';
export const SERVER_PATH_TEMP =  '/assets/temp';
export const SERVER_PATH_PROJECTS = '/assets/projects';

export const FILE_SUBTITLES = 'subtitles.pss';

export const PROJECTS = 'projects';
export const FILES = 'files';
export const TEMP = 'temp';