import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ORM_CONFIG, EMAIL_CONFIG } from './config';
import { AppController } from './app.controller';
import { EventsModule } from './events/events.module';
import { VideoModule } from './video/video.module';
import { ProjectModule } from './project/project.module';
import { SubtitleModule } from './subtitle/subtitle.module';
import { SettingsModule } from './settings/settings.module';
import { HotkeysModule } from './hotkeys/hotkeys.module';
import { ErrorsModule } from './errors/errors.module';
import { GlobalErrorsModule } from './globalErrors/globalErrors.module';
import { GlobalSettingsModule } from './globalSettings/globalSettings.module';
import { VideoGateway } from './video/video.gateway';
import { ErrorsInterceptor } from './common/interceptors';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { GuidelinesModule } from './guidelines/guidelines.module';
import { MailerModule } from '@nest-modules/mailer';

@Module({
  imports: [
    TypeOrmModule.forRoot(ORM_CONFIG),
    MailerModule.forRoot(EMAIL_CONFIG),
    EventsModule,
    VideoModule,
    ProjectModule,
    SubtitleModule,
    SettingsModule,
    HotkeysModule,
    ErrorsModule,
    GlobalSettingsModule,
    AuthModule,
    UserModule,
    GuidelinesModule,
    GlobalErrorsModule,
  ],
  controllers: [AppController],
  providers: [
    VideoGateway,
    {
      provide: APP_INTERCEPTOR,
      useClass: ErrorsInterceptor,
    },
  ],
})
export class ApplicationModule { }
