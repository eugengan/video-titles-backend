import { IsString, IsEmail } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class LoginDto {
  @IsEmail()
  @ApiModelProperty({ example: 'mail@example.com' })
  readonly email: string;

  @IsString()
  @ApiModelProperty({ example: 'password1234' })
  readonly password: string;
}
