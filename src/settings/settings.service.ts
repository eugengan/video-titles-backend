import { Injectable } from '@nestjs/common';
import { Repository, DeleteResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Settings } from './settings.entity';
import { CreateSettingsDto } from './dto/create-settings.dto';
import { UpdateSettingsDto } from './dto/update-settings.dto';

const relations = ['guideline'];

@Injectable()
export class SettingsService {
  constructor(
    @InjectRepository(Settings)
    private readonly settingsRepository: Repository<Settings>,
  ) { }

  create(createProjectSettingsDto: CreateSettingsDto): Promise<Settings> {
    const setting = { ...new Settings(), ...createProjectSettingsDto };
    return this.settingsRepository.save(setting);
  }

  async fetchAll(): Promise<CreateSettingsDto[]> {
    return this.settingsRepository.find({ relations });
  }

  findById(id: string | number): Promise<Settings> {
    return this.settingsRepository.findOne(id, { relations });
  }

  findByName(name: string): Promise<Settings> {
    return this.settingsRepository.findOne({ name }, { relations });
  }

  async updateById(id: string | number, updateProjectSettingsDto: UpdateSettingsDto): Promise<Settings> {
    await this.settingsRepository.update(id, updateProjectSettingsDto);
    return this.findById(id);
  }

  async removeById(id: number | string): Promise<DeleteResult> {
    return this.settingsRepository.delete(id);
  }
}