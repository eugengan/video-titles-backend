import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  HttpCode,
  UseGuards,
} from '@nestjs/common';
import { Settings } from './settings.entity';
import { SettingsService } from './settings.service';
import { CreateSettingsDto } from './dto/create-settings.dto';
import { UpdateSettingsDto } from './dto/update-settings.dto';
import { DeleteResult } from 'typeorm';
import { ApiOperation, ApiUseTags, ApiResponse, ApiImplicitParam, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/roles.guard';
import { UserRole } from '../common/enums/user-role.enum';
import { AcceptableRoles } from '../common/decorators/roles.decorator';

@Controller('settings')
@ApiUseTags('settings')
@UseGuards(AuthGuard('jwt'), RolesGuard)
@ApiBearerAuth()
export class SettingsController {
  constructor(
    private readonly settingsService: SettingsService,
  ) { }

  @Get()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Get all settings' })
  async fetchAll(): Promise<CreateSettingsDto[]> {
    return this.settingsService.fetchAll();
  }

  @Get('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Get setting by id' })
  @ApiImplicitParam({name: 'id'})
  fetchById(@Param('id') id: number | string): Promise<Settings> {
    return this.settingsService.findById(id);
  }

  @Post()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Create a new setting' })
  async create(@Body() createProjectSettingsDto: CreateSettingsDto): Promise<Settings> {
    return this.settingsService.create({
      ...createProjectSettingsDto,
    } as Settings);
  }

  @Put('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Update setting by id' })
  @ApiImplicitParam({name: 'id'})
  update(
    @Param('id') id: string | number,
    @Body() updateProjectSettingsDto: UpdateSettingsDto,
  ): Promise<Settings> {
    return this.settingsService.updateById(id, updateProjectSettingsDto);
  }

  @Delete('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @HttpCode(204)
  @ApiOperation({ title: 'Delete setting by id' })
  @ApiImplicitParam({name: 'id'})
  @ApiResponse({ status: 204, description: 'Successfully deleted setting' })
  remove(
    @Param('id') id: number | string,
    ): Promise<DeleteResult> {
    return this.settingsService.removeById(id);
  }
}