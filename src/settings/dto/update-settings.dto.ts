import { IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import {CreateSettingsDto} from './create-settings.dto';

export class UpdateSettingsDto extends CreateSettingsDto {
  @IsNumber()
  @ApiModelProperty()
  readonly id: number;
}