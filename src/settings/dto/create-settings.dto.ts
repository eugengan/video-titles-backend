import { IsString, IsOptional, ValidateNested } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { UpdateGuideLinesDto } from '../../guidelines/dto/update-guidelines.dto';

export class CreateSettingsDto {
  @IsString()
  @ApiModelProperty()
  readonly name: string;

  @IsString()
  @ApiModelProperty()
  readonly value: string;

  @IsOptional()
  @ValidateNested()
  @ApiModelProperty({ type: UpdateGuideLinesDto })
  readonly guideline?: UpdateGuideLinesDto;
}