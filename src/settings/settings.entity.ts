import { Entity, ManyToOne, Column } from 'typeorm';
import { BaseEntity } from '../common/entity/base.entity';
import { GuideLines } from '../guidelines/guidelines.entity';

@Entity()
export class Settings extends BaseEntity{
  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false })
  value: string;

  @ManyToOne(type => GuideLines, guideline => guideline.settings)
  guideline: GuideLines;
}