import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { User } from './user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { PasswordService } from '../common/services/password.service';
import { ResponseUserInfoDto } from './dto/response-user.dto';
import { QueryOrder } from '../common/enums/query-order.enum';
import { FilterUserDto } from './dto/filter-user-type.dto';
import { UserRole } from '../common/enums/user-role.enum';
import { UpdateUserDto } from './dto/update-user.dto';
import { EmailService } from '../common/services/email.service';

@Injectable()
export class UserService {
  constructor(
    private readonly passwordService: PasswordService,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly emailService: EmailService,
  ) {}

  async findAll(query: FilterUserDto, userRole: UserRole): Promise<User[]> {
    const { name, role, orderType = QueryOrder.DESC, orderBy = 'name' } = query;

    const sqlQuery = this.userRepository.createQueryBuilder('users');

    if (name) {
      sqlQuery.andWhere('users.name LIKE :name',
        { name: `%${name}%` });
    }
    if (role) {
      sqlQuery.andWhere('users.role LIKE :role',
        { role: `%${role}%` });
    }

    if (userRole !== UserRole.SUPER_ADMIN) {
      sqlQuery.andWhere('users.role LIKE :role',
        { role: `%${UserRole.USER}%` });
    }
    const order = {};
    order[`users.${orderBy}`] = orderType || QueryOrder.DESC;

    return sqlQuery
      .orderBy(order)
      .getMany();
  }

  async findById(id: number | string): Promise<User> {
    return this.userRepository.findOne(id);
  }

  async findByEmail(email: string): Promise<User> {
    return this.userRepository.findOne({ email });
  }

  findByEmailAuth(email: string): Promise<User> {
    return this.userRepository.findOne({ email }, {
      select: ['passwordHash', 'id', 'status', 'name', 'email'],
    });
  }

  async create(userDto: CreateUserDto, req): Promise<ResponseUserInfoDto> {
    const passwordHash = await this.passwordService.generatePassword(userDto.password);
    const {password, ...userForSaving} = userDto;
    const user = await this.userRepository.save({ ...new User(), ...userForSaving, passwordHash });
    this.emailService.userCreation({
      email: user.email,
      password: userDto.password,
      name: user.name,
      url: `${req.protocol}://${req.headers.host}/login`,
    });
    return this.excludePasswordByCreating(user);
  }

  excludePasswordByCreating(user: User): ResponseUserInfoDto {
    const { passwordHash, ...filtered } = user;
    return filtered;
  }

  async update(updateDto: UpdateUserDto, id: number): Promise<User> {
    await this.userRepository.update({ id }, updateDto);
    return this.userRepository.findOne({ id });
  }

  remove(id: number): Promise<DeleteResult> {
    return this.userRepository.delete({ id });
  }

  isCanManipulateUser(userRole: string, roleOnChange: string): boolean{
    if (roleOnChange === UserRole.ADMIN && userRole !== UserRole.SUPER_ADMIN ||
        roleOnChange === UserRole.SUPER_ADMIN && userRole !== UserRole.SUPER_ADMIN) {
          return false;
      }
    return true;
  }
}
