import { IsString, IsEmail, IsOptional, IsEnum } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserStatus } from '../../common/enums/user-status.enum';
import { UserRole } from '../../common/enums/user-role.enum';
import { UpdateProjectDto } from '../../project/dto/update-project.dto';

export class CreateUserDto {
  @IsEmail()
  @ApiModelProperty()
  readonly email: string;

  @IsString()
  @ApiModelProperty()
  readonly password: string;

  @IsString()
  @ApiModelProperty()
  readonly name: string;

  @IsEnum(UserRole)
  @IsOptional()
  @ApiModelProperty({ enum: UserRole })
  readonly role?: UserRole;

  @IsEnum(UserStatus)
  @IsOptional()
  @ApiModelProperty({ enum: UserStatus, required: false })
  readonly status?: UserStatus;

  @IsOptional()
  @ApiModelProperty({ type: UpdateProjectDto })
  readonly assignedProject?: UpdateProjectDto[];

  @IsOptional()
  @ApiModelProperty({ type: UpdateProjectDto })
  readonly createdProject?: UpdateProjectDto[];
}
