import { IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { User } from '../user.entity';

export class ResponseByCreateUserDto extends User {
  @IsString()
  @ApiModelProperty()
  readonly password: string;
}
