import { IsString, IsEmail, IsOptional, IsEnum, IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserStatus } from '../../common/enums/user-status.enum';
import { UserRole } from '../../common/enums/user-role.enum';
import { UpdateProjectDto } from '../../project/dto/update-project.dto';

export class UpdateUserDto  {
  @IsNumber()
  @ApiModelProperty()
  readonly id: number;

  @IsEmail()
  @IsOptional()
  @ApiModelProperty()
  readonly email?: string;

  @IsString()
  @IsOptional()
  @ApiModelProperty()
  readonly password?: string;

  @IsString()
  @IsOptional()
  @ApiModelProperty()
  readonly passwordHash?: string;

  @IsString()
  @IsOptional()
  @ApiModelProperty()
  readonly name?: string;

  @IsEnum(UserRole)
  @IsOptional()
  @ApiModelProperty({ enum: UserRole })
  readonly role?: UserRole;

  @IsEnum(UserStatus)
  @IsOptional()
  @ApiModelProperty({ enum: UserStatus, required: false })
  readonly status?: UserStatus;

  @IsOptional()
  @ApiModelProperty({ type: UpdateProjectDto })
  readonly assignedProject?: UpdateProjectDto[];

  @IsOptional()
  @ApiModelProperty({ type: UpdateProjectDto })
  readonly createdProject?: UpdateProjectDto[];
}