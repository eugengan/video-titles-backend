import { IsString, IsOptional, IsEnum } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { QueryOrder } from '../../common/enums/query-order.enum';
import { UserRole } from '../../common/enums/user-role.enum';

export class FilterUserDto {
    @IsString() @IsOptional() @ApiModelPropertyOptional() readonly name?: string;
    @IsString() @IsOptional() @ApiModelPropertyOptional() readonly orderBy?: string;
    @IsEnum(QueryOrder) @IsOptional() @ApiModelPropertyOptional() readonly orderType?: QueryOrder;
    @IsEnum(UserRole) @IsOptional() @ApiModelPropertyOptional() readonly role?: UserRole;
}
