import { ApiModelProperty } from '@nestjs/swagger';
import { UserStatus } from '../../common/enums/user-status.enum';
import { UserRole } from '../../common/enums/user-role.enum';

export class ResponseUserInfoDto {

  @ApiModelProperty({ example: 1 })
  readonly id: number;

  @ApiModelProperty({ example: 'mail@example.com' })
  readonly email: string;

  @ApiModelProperty({ example: 'Admin' })
  readonly name: string;

  @ApiModelProperty({ enum: UserRole, example: UserRole.USER })
  readonly role: UserRole;

  @ApiModelProperty({ enum: UserStatus, example: UserStatus.ACTIVE })
  readonly status: UserStatus;
}
