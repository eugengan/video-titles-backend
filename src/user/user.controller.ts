import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
  Patch,
} from '@nestjs/common';

import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiUseTags,
} from '@nestjs/swagger';

import { User } from './user.entity';
import { UserService } from './user.service';
import { ResponseUserInfoDto } from './dto/response-user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/roles.guard';
import { UserRole } from '../common/enums/user-role.enum';
import { AcceptableRoles } from '../common/decorators/roles.decorator';
import { FilterUserDto } from './dto/filter-user-type.dto';
import { UpdatePartialUserDto } from './dto/update-partial-user.dto';
import { DeleteResponseDto } from '../common/dto/delete.response.dto';
import { PasswordService } from '../common/services/password.service';

@ApiUseTags('user')
@Controller('user')
@UseGuards(AuthGuard('jwt'), RolesGuard)
@ApiBearerAuth()
export class UserController {

  constructor(
    private readonly userService: UserService,
    private readonly passwordService: PasswordService,
  ){}

  @Get()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Get all users' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto, isArray: true })
  fetchAll(@Req() req, @Query() query: FilterUserDto): Promise<ResponseUserInfoDto[]> {
    if (!req.user) throw new HttpException('You not authorized', HttpStatus.BAD_REQUEST);
    const { role } = req.user;
    return this.userService.findAll(query, role);
  }

  @Get('/me')
  @ApiOperation({ title: 'Get user by token' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto })
  fetchUserByToken(@Req() req): Promise<User> {
    if (!req.user) throw new HttpException('You not authorized', HttpStatus.BAD_REQUEST);
    const { id } = req.user;
    return this.userService.findById(id);
  }

  @Get('/email')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Get user by email' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto })
  fetchUserByEmail(@Query('email') email: string): Promise<User> {
    const user = this.userService.findByEmail(email);
    if (!user) throw new HttpException('Can\'t find user with this email', HttpStatus.BAD_REQUEST);
    return user;
  }

  @Get('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Get user by id' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto })
  async fetchUserById(@Param('id') userId: number): Promise<User> {
    const user = await this.userService.findById(userId);
    if (!user) throw new HttpException('Can\'t find user with this id', HttpStatus.BAD_REQUEST);
    return user;
  }

  @Post()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Create / register a new user' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto })
  async create(@Req() req, @Body() userDto: CreateUserDto): Promise<ResponseUserInfoDto> {
    const user = await this.userService.findByEmail(userDto.email);
    if (user) throw new HttpException('User with this email already registered', HttpStatus.BAD_REQUEST);

    if (!this.userService.isCanManipulateUser(req.user.role, userDto.role)) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    return this.userService.create(userDto, req);
  }

  @Put('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Update an existing user' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto })
  async update(
    @Req() req,
    @Body() userDto: UpdateUserDto,
    @Param('id') userId: number,
  ): Promise<User> {
    const user = await this.userService.findById(userId);
    if (!user) throw new HttpException('Can\'t find user with this id', HttpStatus.BAD_REQUEST);

    if (!this.userService.isCanManipulateUser(req.user.role, userDto.role)) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    return this.userService.update(userDto, userId);
  }

  @Patch('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Update an existing user' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto })
  async updatePartial(
    @Req() req,
    @Body() userDto: UpdatePartialUserDto,
    @Param('id') userId: number,
  ): Promise<User> {
    const user = await this.userService.findById(userId);
    if (!user) throw new HttpException('Can\'t find user with this id', HttpStatus.BAD_REQUEST);

    if (!this.userService.isCanManipulateUser(req.user.role, user.role)) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }
    if (userDto.password) {
      const passwordHash = await this.passwordService.generatePassword(userDto.password);
      return this.userService.update({ ...userDto, passwordHash }, userId);
    }
    return this.userService.update(userDto, userId);
  }

  @Delete('/:id')
  @ApiOperation({ title: 'Delete user' })
  @ApiResponse({ status: 200, description: 'Successfully deleted user' })
  async remove(
    @Req() req,
    @Param('id') userId: number,
  ): Promise<DeleteResponseDto> {
    const user = await this.userService.findById(userId);
    if (!user) throw new HttpException('Can\'t find user with this id', HttpStatus.BAD_REQUEST);

    if (!this.userService.isCanManipulateUser(req.user.role, user.role)) {
      throw new HttpException('Permission denied', HttpStatus.FORBIDDEN);
    }

    await this.userService.remove(userId);
    return { success: true, id: userId };
  }
}
