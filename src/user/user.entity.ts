import { Entity, Column, OneToMany } from 'typeorm';
import { Exclude } from 'class-transformer';
import { BaseEntity } from '../common/entity/base.entity';
import { UserStatus } from '../common/enums/user-status.enum';
import { UserRole } from '../common/enums/user-role.enum';
import { Project } from '../project/project.entity';

@Entity()
export class User extends BaseEntity {

  @Column({ length: 255, select: false, name: 'password_hash' })
  @Exclude()
  passwordHash: string;

  @Column({ unique: true, length: 255 })
  email: string;

  @Column({ length: 255, nullable: true, name: 'name' })
  name: string;

  @Column({ type: 'enum', enum: UserRole, default: UserRole.USER })
  role: UserRole;

  @Column({ type: 'enum', enum: UserStatus, default: UserStatus.ACTIVE })
  status: UserStatus;

  @OneToMany(type => Project, project => project.assignedUser)
  assignedProject?: Project[];

  @OneToMany(type => Project, project => project.createdBy)
  createdProject?: Project[];
}
