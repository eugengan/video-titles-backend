import { Entity, ManyToOne, Column } from 'typeorm';
import { BaseEntity } from '../common/entity/base.entity';
import { Project } from '../project/project.entity';
import { SubtitlesDto } from '../project/dto/titles.dto';

@Entity()
export class Subtitles extends BaseEntity{
  @Column({ nullable: true, type: 'json' })
  originalTitles?: SubtitlesDto[];

  @Column({ nullable: true, type: 'json' })
  titlesList?: SubtitlesDto[];

  @ManyToOne(type => Project, project => project.titles)
  projectId: Project;
}