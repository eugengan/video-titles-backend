import {
  Controller,
  Get,
  Post,
  Body,
  Delete,
  HttpCode,
  Param,
  Put,
  Req,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
  HttpException,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import {
  ApiOperation,
  ApiUseTags,
  ApiImplicitParam,
  ApiResponse,
  ApiConsumes,
  ApiImplicitFile,
  ApiBearerAuth,
} from '@nestjs/swagger';

import { SubtitleService } from './subtitle.service';

import { SpellCheckResultDto } from './dto/spell-check-result.dto';
import { SubtitlesDto } from '../project/dto/titles.dto';
import { CreateSubtitleDto } from './dto/create-subtitle.dto';
import { DeleteResult } from 'typeorm';
import { Subtitles } from './subtitle.entity';
import { UpdateSubtitleDto } from './dto/update-subtitle.dto';
import parseSubtitlesData from '../common/utils/parseSubtitlesData';
import { DownloadSubtitleDto } from './dto/download-subtitle.dto';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/roles.guard';
import { UserRole } from '../common/enums/user-role.enum';
import { AcceptableRoles } from '../common/decorators/roles.decorator';

@Controller('subtitle')
@ApiUseTags('subtitle')
@UseGuards(AuthGuard('jwt'), RolesGuard)
@ApiBearerAuth()
export class SubtitleController {
  constructor(
    private readonly subtitlesService: SubtitleService,
  ){}
  @Get()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Get all subtitles' })
  async fetchAll(): Promise<Subtitles[]> {
    return this.subtitlesService.fetchAll();
  }

  @Get('/:id')
  @ApiOperation({ title: 'Get subtitle by id' })
  @ApiImplicitParam({name: 'id'})
  fetchById(@Param('id') id: number | string): Promise<Subtitles> {
    return this.subtitlesService.findById(id);
  }

  @Get('/project/:id')
  @ApiOperation({ title: 'Get subtitle by project id' })
  @ApiImplicitParam({name: 'id'})
  fetchByProjectId(@Param('id') id: number | string): Promise<Subtitles[]> {
    return this.subtitlesService.fetchAllByProjectId(id);
  }

  @Post()
  @ApiOperation({ title: 'Create a new subtitle' })
  async create(@Body() subtitles: CreateSubtitleDto): Promise<Subtitles> {
    return this.subtitlesService.create(subtitles);
  }

  @Post('/spell-check')
  @ApiOperation({ title: 'Spell check subtitle' })
  @ApiResponse({ status: 200, type: SpellCheckResultDto })
  spellCheck(@Body() title: SubtitlesDto): SpellCheckResultDto {
    return this.subtitlesService.spellCheck(title);
  }

  @Put('/:id')
  @ApiOperation({ title: 'Update subtitle by id' })
  @ApiImplicitParam({name: 'id'})
  update(
    @Param('id') id: string | number,
    @Body() updateProjectSettingsDto: UpdateSubtitleDto,
  ): Promise<Subtitles> {
    return this.subtitlesService.updateById(id, updateProjectSettingsDto);
  }

  @Delete('/:id')
  @HttpCode(204)
  @ApiOperation({ title: 'Delete subtitle by id' })
  @ApiImplicitParam({name: 'id'})
  @ApiResponse({ status: 204, description: 'Successfully deleted subtitle' })
  remove(
    @Param('id') id: number | string,
    ): Promise<DeleteResult> {
    return this.subtitlesService.removeById(id);
  }

  @Post('/upload-subtitles')
  @UseInterceptors(FileInterceptor('file'))
  @ApiOperation({ title: 'Upload subtitles '})
  @ApiResponse({ status: 200, type: SubtitlesDto })
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true, description: 'Subtitles file for project' })
  async uploadSubtitles(
    @Body() body,
    @UploadedFile() file,
  ): Promise<SubtitlesDto[]> {
    const subtitles = file.buffer.toString();
    if (!subtitles.length) {
      throw new HttpException(`Subtitles is empty`, HttpStatus.BAD_REQUEST);
    }

    return parseSubtitlesData(subtitles);
  }

  @Post('/download-subtitles')
  @ApiOperation({ title: 'Upload subtitles '})
  @ApiResponse({ status: 200, description: 'Created subtitles file' })
  downloadSubtitles(@Req() req, @Body() data: DownloadSubtitleDto): string {
    return this.subtitlesService.downloadSubtitles(data, req.headers.origin);
  }
}