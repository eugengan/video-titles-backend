import { Module } from '@nestjs/common';
import { SubtitleController } from './subtitle.controller';
import { SubtitleService } from './subtitle.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Subtitles } from './subtitle.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Subtitles]),
  ],
  controllers: [SubtitleController],
  providers: [SubtitleService],
  exports: [SubtitleService],
})
export class SubtitleModule { }