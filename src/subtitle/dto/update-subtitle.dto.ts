import { IsNumber, ValidateNested, IsArray, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Project } from '../../project/project.entity';
import { SubtitlesDto } from '../../project/dto/titles.dto';

export class UpdateSubtitleDto {
  @IsNumber()
  @ApiModelProperty()
  readonly id: number;

  @IsOptional()
  @IsArray()
  @ValidateNested()
  @ApiModelProperty()
  readonly originalTitles?: SubtitlesDto[];

  @IsOptional()
  @IsArray()
  @ValidateNested()
  @ApiModelProperty()
  readonly titlesList?: SubtitlesDto[];

  @IsNumber()
  @ApiModelProperty({ type : Project })
  readonly projectId: Project;
}