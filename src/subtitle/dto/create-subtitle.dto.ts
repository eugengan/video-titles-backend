import { IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Project } from '../../project/project.entity';
import { CreateSubtitleWithProjectDto } from './create-subtitle-with-project.dto';

export class CreateSubtitleDto extends CreateSubtitleWithProjectDto {

  @IsNumber()
  @ApiModelProperty({ type : Project })
  readonly projectId: Project;
}