import { IsArray } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { SubtitlesDto } from '../../project/dto/titles.dto';

export class SpellCheckResultDto extends SubtitlesDto{

  @IsArray()
  @ApiModelProperty()
  readonly failWords: Array<string>;
}