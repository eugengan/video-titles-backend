import { ValidateNested, IsArray, IsOptional, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { SubtitlesDto } from '../../project/dto/titles.dto';

export class DownloadSubtitleDto {
  @IsOptional()
  @IsArray()
  @ValidateNested()
  @ApiModelProperty()
  readonly titlesList: SubtitlesDto[];

  @IsString()
  @ApiModelProperty()
  readonly projectDir: string;
}