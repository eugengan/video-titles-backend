import { ValidateNested, IsArray, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { SubtitlesDto } from '../../project/dto/titles.dto';

export class CreateSubtitleWithProjectDto {
  @IsOptional()
  @IsArray()
  @ValidateNested()
  @ApiModelProperty({ type: SubtitlesDto, isArray: true })
  readonly originalTitles?: SubtitlesDto[];

  @IsOptional()
  @IsArray()
  @ValidateNested()
  @ApiModelProperty({ type: SubtitlesDto, isArray: true })
  readonly titlesList?: SubtitlesDto[];
}