import { Injectable } from '@nestjs/common';
import { SubtitlesDto } from '../project/dto/titles.dto';
import spellCheckService from '../common/services/spell-check.service';
import { CreateSubtitleDto } from './dto/create-subtitle.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Subtitles } from './subtitle.entity';
import { Repository, DeleteResult } from 'typeorm';
import { VERSION_LIMIT } from '../common/constants/subtitle.constanst';
import { UpdateSubtitleDto } from './dto/update-subtitle.dto';
import saveTitlesInFileService from '../common/services/saveTitlesInFile.service';
import { DownloadSubtitleDto } from './dto/download-subtitle.dto';

@Injectable()
export class SubtitleService {
  constructor(
    @InjectRepository(Subtitles)
    private readonly subtilesRepository: Repository<Subtitles>,
  ) { }

  create(createSubtitleDto: CreateSubtitleDto): Promise<Subtitles> {
    return this.subtilesRepository.save({
      ...new SubtitlesDto(),
      ...createSubtitleDto,
    });
  }

  fetchAll(): Promise<Subtitles[]> {
    return this.subtilesRepository.find();
  }

  findById(id: string | number): Promise<Subtitles> {
    return this.subtilesRepository.findOne(id);
  }
  fetchAllByProjectId(id: string | number): Promise<Subtitles[]> {
    return this.subtilesRepository
      .createQueryBuilder('titles')
      .where('titles.projectId =:id', { id })
      .orderBy({ 'titles.updatedAt': 'DESC' })
      .take(VERSION_LIMIT)
      .getMany();
  }

  async updateById(id: string | number, updateSubtitle: UpdateSubtitleDto): Promise<Subtitles> {
    await this.subtilesRepository.update(id, updateSubtitle);
    return this.findById(id);
  }

  removeById(id: number | string): Promise<DeleteResult> {
    return this.subtilesRepository.delete(id);
  }

  spellCheck(title: SubtitlesDto): any {
    return spellCheckService(title);
  }
  downloadSubtitles(data: DownloadSubtitleDto, host: string): string {
    const { projectDir , titlesList } = data;
    return saveTitlesInFileService(titlesList, projectDir, host);
  }
}