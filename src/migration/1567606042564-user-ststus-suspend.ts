import { MigrationInterface, QueryRunner } from "typeorm";

export class UserStatusSuspend1567606042564 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {   
       await queryRunner.query("ALTER TABLE `user` MODIFY COLUMN `status` enum ('ACTIVE', 'SUSPEND') NOT NULL DEFAULT 'ACTIVE'");           
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query("ALTER TABLE `user` MODIFY COLUMN `status` enum ('ACTIVE', 'INACTIVE') NOT NULL DEFAULT 'ACTIVE'");      
    }

}
