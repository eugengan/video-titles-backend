import {MigrationInterface, QueryRunner} from "typeorm";

export class init1558342405425 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `errors` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, `value` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `global_settings` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, `value` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `settings` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, `value` varchar(255) NOT NULL, `projectIdId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `subtitles` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `originalTitles` json NULL, `titlesList` json NULL, `projectIdId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `project` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, `videoFile` varchar(255) NOT NULL, `audioFiles` text NOT NULL, `metadata` json NOT NULL, `projectDir` varchar(255) NOT NULL, UNIQUE INDEX `IDX_dedfea394088ed136ddadeee89` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `hotkeys` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, `value` varchar(255) NOT NULL, `projectIdId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `settings` ADD CONSTRAINT `FK_818f2e0171e5be3ad8840d82249` FOREIGN KEY (`projectIdId`) REFERENCES `project`(`id`)");
        await queryRunner.query("ALTER TABLE `subtitles` ADD CONSTRAINT `FK_42a364afd56cb5432d83fe56619` FOREIGN KEY (`projectIdId`) REFERENCES `project`(`id`)");
        await queryRunner.query("ALTER TABLE `hotkeys` ADD CONSTRAINT `FK_14990d1382d808a8032a3b8a966` FOREIGN KEY (`projectIdId`) REFERENCES `project`(`id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `hotkeys` DROP FOREIGN KEY `FK_14990d1382d808a8032a3b8a966`");
        await queryRunner.query("ALTER TABLE `subtitles` DROP FOREIGN KEY `FK_42a364afd56cb5432d83fe56619`");
        await queryRunner.query("ALTER TABLE `settings` DROP FOREIGN KEY `FK_818f2e0171e5be3ad8840d82249`");
        await queryRunner.query("DROP TABLE `hotkeys`");
        await queryRunner.query("DROP INDEX `IDX_dedfea394088ed136ddadeee89` ON `project`");
        await queryRunner.query("DROP TABLE `project`");
        await queryRunner.query("DROP TABLE `subtitles`");
        await queryRunner.query("DROP TABLE `settings`");
        await queryRunner.query("DROP TABLE `global_settings`");
        await queryRunner.query("DROP TABLE `errors`");
    }

}
