import {MigrationInterface, QueryRunner} from "typeorm";

export class globalErrors1568383347189 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `errors` DROP FOREIGN KEY `FK_0c47e8096caa236c8d3b3d67547`");
        await queryRunner.query("ALTER TABLE `settings` DROP FOREIGN KEY `FK_cdd3baf3d1914e388b0af66a8f9`");
        await queryRunner.query("ALTER TABLE `project` DROP FOREIGN KEY `FK_e0ba07a332a94f4cee066f64305`");
        await queryRunner.query("ALTER TABLE `settings` DROP FOREIGN KEY `FK_818f2e0171e5be3ad8840d82249`");
        await queryRunner.query("CREATE TABLE `global_errors` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, `value` varchar(255) NOT NULL, `permissions` enum ('Don''t check', 'Allow submit but warn first', 'Warn and not allow to submit') NOT NULL DEFAULT 'Don''t check', `color` enum ('red', 'orange') NOT NULL DEFAULT 'red', `guidelineId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `errors` DROP COLUMN `permissions`");
        await queryRunner.query("ALTER TABLE `errors` DROP COLUMN `color`");
        await queryRunner.query("ALTER TABLE `errors` DROP COLUMN `guidelineId`");
        await queryRunner.query("ALTER TABLE `settings` DROP COLUMN `guidelineId`");
        await queryRunner.query("ALTER TABLE `project` DROP COLUMN `guidelineId`");
        await queryRunner.query("ALTER TABLE `settings` DROP COLUMN `projectIdId`");
        await queryRunner.query("ALTER TABLE `settings` ADD `projectIdId` int NULL");
        await queryRunner.query("ALTER TABLE `settings` ADD `guidelineId` int NULL");
        await queryRunner.query("ALTER TABLE `project` ADD `guidelineId` int NULL");
        await queryRunner.query("ALTER TABLE `errors` ADD `permissions` enum ('Don''t check', 'Allow submit but warn first', 'Warn and not allow to submit') NOT NULL DEFAULT 'Don''t check'");
        await queryRunner.query("ALTER TABLE `errors` ADD `color` enum ('red', 'orange') NOT NULL DEFAULT 'red'");
        await queryRunner.query("ALTER TABLE `errors` ADD `guidelineId` int NULL");
        await queryRunner.query("ALTER TABLE `settings` ADD CONSTRAINT `FK_818f2e0171e5be3ad8840d82249` FOREIGN KEY (`projectIdId`) REFERENCES `project`(`id`)");
        await queryRunner.query("ALTER TABLE `settings` ADD CONSTRAINT `FK_cdd3baf3d1914e388b0af66a8f9` FOREIGN KEY (`guidelineId`) REFERENCES `guide_lines`(`id`)");
        await queryRunner.query("ALTER TABLE `project` ADD CONSTRAINT `FK_e0ba07a332a94f4cee066f64305` FOREIGN KEY (`guidelineId`) REFERENCES `guide_lines`(`id`)");
        await queryRunner.query("ALTER TABLE `errors` ADD CONSTRAINT `FK_0c47e8096caa236c8d3b3d67547` FOREIGN KEY (`guidelineId`) REFERENCES `guide_lines`(`id`)");
        await queryRunner.query("ALTER TABLE `global_errors` ADD CONSTRAINT `FK_88f7d7dce7ca12594227b4dcf8e` FOREIGN KEY (`guidelineId`) REFERENCES `guide_lines`(`id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `global_errors` DROP FOREIGN KEY `FK_88f7d7dce7ca12594227b4dcf8e`");
        await queryRunner.query("ALTER TABLE `errors` DROP FOREIGN KEY `FK_0c47e8096caa236c8d3b3d67547`");
        await queryRunner.query("ALTER TABLE `project` DROP FOREIGN KEY `FK_e0ba07a332a94f4cee066f64305`");
        await queryRunner.query("ALTER TABLE `settings` DROP FOREIGN KEY `FK_cdd3baf3d1914e388b0af66a8f9`");
        await queryRunner.query("ALTER TABLE `settings` DROP FOREIGN KEY `FK_818f2e0171e5be3ad8840d82249`");
        await queryRunner.query("ALTER TABLE `errors` DROP COLUMN `guidelineId`");
        await queryRunner.query("ALTER TABLE `errors` DROP COLUMN `color`");
        await queryRunner.query("ALTER TABLE `errors` DROP COLUMN `permissions`");
        await queryRunner.query("ALTER TABLE `project` DROP COLUMN `guidelineId`");
        await queryRunner.query("ALTER TABLE `settings` DROP COLUMN `guidelineId`");
        await queryRunner.query("ALTER TABLE `settings` DROP COLUMN `projectIdId`");
        await queryRunner.query("ALTER TABLE `settings` ADD `projectIdId` int NULL");
        await queryRunner.query("ALTER TABLE `project` ADD `guidelineId` int NULL");
        await queryRunner.query("ALTER TABLE `settings` ADD `guidelineId` int NULL");
        await queryRunner.query("ALTER TABLE `errors` ADD `guidelineId` int NULL");
        await queryRunner.query("ALTER TABLE `errors` ADD `color` enum ('red', 'orange') NOT NULL DEFAULT 'red'");
        await queryRunner.query("ALTER TABLE `errors` ADD `permissions` enum ('Don''t check', 'Allow submit but warn first', 'Warn and not allow to submit') NOT NULL DEFAULT 'Don''t check'");
        await queryRunner.query("DROP TABLE `global_errors`");
        await queryRunner.query("ALTER TABLE `settings` ADD CONSTRAINT `FK_818f2e0171e5be3ad8840d82249` FOREIGN KEY (`projectIdId`) REFERENCES `project`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
        await queryRunner.query("ALTER TABLE `project` ADD CONSTRAINT `FK_e0ba07a332a94f4cee066f64305` FOREIGN KEY (`guidelineId`) REFERENCES `guide_lines`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
        await queryRunner.query("ALTER TABLE `settings` ADD CONSTRAINT `FK_cdd3baf3d1914e388b0af66a8f9` FOREIGN KEY (`guidelineId`) REFERENCES `guide_lines`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
        await queryRunner.query("ALTER TABLE `errors` ADD CONSTRAINT `FK_0c47e8096caa236c8d3b3d67547` FOREIGN KEY (`guidelineId`) REFERENCES `guide_lines`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
    }

}
