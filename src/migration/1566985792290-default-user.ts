import { MigrationInterface, QueryRunner } from "typeorm";

export class DefaultUser1566985792290 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
       await queryRunner.query("Insert into user (email, password_hash, name, role, status) values ('admin@mail.com', '$2b$10$C/2QwidmBcNkaO2LWVW9Vuo5QJo5PLBXN26qwjxl37OPwix2rfvcC', 'admin', 'ADMIN', 'ACTIVE')");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query("Delete from user where email='admin@mail.com'");           
    }

}
