import { MigrationInterface, QueryRunner } from "typeorm";

export class UserSuperAdminClient1567606037913 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {   
       await queryRunner.query("ALTER TABLE `user` MODIFY COLUMN `role` enum ('USER', 'ADMIN', 'SUPER_ADMIN', 'CLIENT') NOT NULL DEFAULT 'USER'");    
       await queryRunner.query("Insert into user (email, password_hash, name, role, status) values ('super_admin@mail.com', '$2b$10$C/2QwidmBcNkaO2LWVW9Vuo5QJo5PLBXN26qwjxl37OPwix2rfvcC', 'super_admin', 'SUPER_ADMIN', 'ACTIVE')");
       await queryRunner.query("Insert into user (email, password_hash, name, role, status) values ('user@mail.com', '$2b$10$C/2QwidmBcNkaO2LWVW9Vuo5QJo5PLBXN26qwjxl37OPwix2rfvcC', 'user', 'USER', 'ACTIVE')");
       await queryRunner.query("Insert into user (email, password_hash, name, role, status) values ('client@mail.com', '$2b$10$C/2QwidmBcNkaO2LWVW9Vuo5QJo5PLBXN26qwjxl37OPwix2rfvcC', 'client', 'CLIENT', 'ACTIVE')");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query("ALTER TABLE `user` MODIFY COLUMN `role` enum ('USER', 'ADMIN') NOT NULL DEFAULT 'USER'");
      await queryRunner.query("Delete from user where email='super_admin@mail.com'");           
      await queryRunner.query("Delete from user where email='user@mail.com'");           
      await queryRunner.query("Delete from user where email='client@mail.com'");           
    }

}
