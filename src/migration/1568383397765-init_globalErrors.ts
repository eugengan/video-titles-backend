import {MigrationInterface, QueryRunner} from "typeorm";

export class initGlobalErrors1568383397765 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (1, 'emptyTitleTextBox', 'Is empty text input in block');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (2, 'excessCharacters', 'The number of characters in the input exceeds the max length of the text');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (3, 'emptyTimecodes', 'Both timecode is empty');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (4, 'notValidTimecodesFlow', 'The start time of the subtitle is set later than the end time of the subtitle');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (5, 'negativeTimecodesValue', 'In one of the timecodes is minus value');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (6, 'overlapSubtitles', 'One subtitle overlap to another');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (7, 'notValidSubtitleDuration', 'The duration of the subtitle is less than the min allowed in settings');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (8, 'spellingMistake', 'In spelling error EAS made');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (9, 'numberCharactersPerLine', 'The number of characters per line exceeds the max length of the text');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (10, 'gapBetweenPreviousSubtitle', 'The gap between previous subtitle is less than allowed in the settings');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (11, 'exceedTextLength', 'The number of characters per line exceeds the max length of the text more then 20%');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (12, 'notLatin', 'Some characters are not LATIN/UNICODE');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (13, 'doubleSpace', 'Double space of the text');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (14, 'notTrimmedText', 'Space at beginning or end of the text');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (15, 'noPunctuation', 'No punctuation at the end of the line');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (16, 'numberLinesSubtitle', 'Max number of lines are exceeded');")
        await queryRunner.query("INSERT INTO `global_errors` (`id`, `name`, `value`) VALUES (17, 'italicFont', 'Font is italic');")
        await queryRunner.query("TRUNCATE TABLE errors;")
    }

    public async down(queryRunner: QueryRunner): Promise<any> { }

}
