import {MigrationInterface, QueryRunner} from "typeorm";

export class removeProjectIdFromSettings1573633899288 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `settings` DROP FOREIGN KEY `FK_818f2e0171e5be3ad8840d82249`");
        await queryRunner.query("ALTER TABLE `settings` DROP COLUMN `projectIdId`");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `settings` ADD `projectIdId` int NULL");
        await queryRunner.query("ALTER TABLE `settings` ADD CONSTRAINT `FK_818f2e0171e5be3ad8840d82249` FOREIGN KEY (`projectIdId`) REFERENCES `project`(`id`)");
    }

}
