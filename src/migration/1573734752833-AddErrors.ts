import {MigrationInterface, QueryRunner} from "typeorm";

export class AddErrors1573734752833 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        queryRunner.query("UPDATE `global_errors` SET `name`='notValidSubtitleDurationMin' where `name`='notValidSubtitleDuration';")
        queryRunner.query("UPDATE `errors` SET `name`='notValidSubtitleDurationMin' where `name`='notValidSubtitleDuration';")
        queryRunner.query("INSERT INTO `global_errors` ( `name`, `value`) VALUES ( 'notValidSubtitleDurationMax', 'The duration of the subtitle is more than the min allowed in settings');")
        queryRunner.query("INSERT INTO `global_errors` ( `name`, `value`) VALUES ( 'raisingValueHigher', 'The raising value of subtitle is higher than allowed in settings');")

        const ids = await queryRunner.query("SELECT DISTINCT guidelineId from errors WHERE guidelineId IS NOT NULL;")
        await Promise.all(ids.map(({ guidelineId }) => {
         queryRunner.query(`INSERT INTO errors ( name, value, guidelineId) VALUES ( 'notValidSubtitleDurationMax', 'The duration of the subtitle is more than the min allowed in settings', ${guidelineId});`)
         queryRunner.query(`INSERT INTO errors ( name, value, guidelineId) VALUES ( 'raisingValueHigher', 'The raising value of subtitle is higher than allowed in settings', ${guidelineId});`)
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<any> {}

}
