import {MigrationInterface, QueryRunner} from "typeorm";

export class Guidelines1567574961906 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `settings` DROP FOREIGN KEY `FK_818f2e0171e5be3ad8840d82249`");
        await queryRunner.query("ALTER TABLE `settings` CHANGE `projectIdId` `guidelineId` int NULL");
        await queryRunner.query("CREATE TABLE `guide_lines` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, UNIQUE INDEX `IDX_cfbe5c6718b7841e341f2f9bc9` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `settings` DROP COLUMN `guidelineId`");
        await queryRunner.query("ALTER TABLE `settings` ADD `projectIdId` int NULL");
        await queryRunner.query("ALTER TABLE `settings` ADD `guidelineId` int NULL");
        await queryRunner.query("ALTER TABLE `project` ADD `guidelineId` int NULL");
        await queryRunner.query("ALTER TABLE `errors` ADD `guidelineId` int NULL");
        await queryRunner.query("ALTER TABLE `settings` ADD CONSTRAINT `FK_818f2e0171e5be3ad8840d82249` FOREIGN KEY (`projectIdId`) REFERENCES `project`(`id`)");
        await queryRunner.query("ALTER TABLE `settings` ADD CONSTRAINT `FK_cdd3baf3d1914e388b0af66a8f9` FOREIGN KEY (`guidelineId`) REFERENCES `guide_lines`(`id`)");
        await queryRunner.query("ALTER TABLE `project` ADD CONSTRAINT `FK_e0ba07a332a94f4cee066f64305` FOREIGN KEY (`guidelineId`) REFERENCES `guide_lines`(`id`)");
        await queryRunner.query("ALTER TABLE `errors` ADD CONSTRAINT `FK_0c47e8096caa236c8d3b3d67547` FOREIGN KEY (`guidelineId`) REFERENCES `guide_lines`(`id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `errors` DROP FOREIGN KEY `FK_0c47e8096caa236c8d3b3d67547`");
        await queryRunner.query("ALTER TABLE `project` DROP FOREIGN KEY `FK_e0ba07a332a94f4cee066f64305`");
        await queryRunner.query("ALTER TABLE `settings` DROP FOREIGN KEY `FK_cdd3baf3d1914e388b0af66a8f9`");
        await queryRunner.query("ALTER TABLE `settings` DROP FOREIGN KEY `FK_818f2e0171e5be3ad8840d82249`");
        await queryRunner.query("ALTER TABLE `errors` DROP COLUMN `guidelineId`");
        await queryRunner.query("ALTER TABLE `project` DROP COLUMN `guidelineId`");
        await queryRunner.query("ALTER TABLE `settings` DROP COLUMN `guidelineId`");
        await queryRunner.query("ALTER TABLE `settings` DROP COLUMN `projectIdId`");
        await queryRunner.query("ALTER TABLE `settings` ADD `guidelineId` int NULL");
        await queryRunner.query("DROP INDEX `IDX_cfbe5c6718b7841e341f2f9bc9` ON `guide_lines`");
        await queryRunner.query("DROP TABLE `guide_lines`");
        await queryRunner.query("ALTER TABLE `settings` CHANGE `guidelineId` `projectIdId` int NULL");
        await queryRunner.query("ALTER TABLE `settings` ADD CONSTRAINT `FK_818f2e0171e5be3ad8840d82249` FOREIGN KEY (`projectIdId`) REFERENCES `project`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT");
    }

}
