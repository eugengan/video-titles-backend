import {MigrationInterface, QueryRunner} from "typeorm";

export class changeSchema1558683976461 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const queryList = [];
        const tables = await queryRunner.query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = 'video_titles'")
        for (let i = 0; i < tables.length; i += 1) {
            queryList.push(queryRunner.query(`ALTER TABLE ${tables[i].TABLE_NAME} CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;`));
        }
        await queryRunner.query("ALTER DATABASE video_titles CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;");
        await Promise.all(queryList);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        
    }

}
