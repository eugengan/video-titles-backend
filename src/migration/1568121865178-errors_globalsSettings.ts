import {MigrationInterface, QueryRunner} from "typeorm";

export class errorsGlobalsSettings1568121865178 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'emptyTitleTextBox', 'Is empty text input in block');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'excessCharacters', 'The number of characters in the input exceeds the max length of the text');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'emptyTimecodes', 'Both timecode is empty');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'notValidTimecodesFlow', 'The start time of the subtitle is set later than the end time of the subtitle');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'negativeTimecodesValue', 'In one of the timecodes is minus value');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'overlapSubtitles', 'One subtitle overlap to another');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'notValidSubtitleDuration', 'The duration of the subtitle is less than the min allowed in settings');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'spellingMistake', 'In spelling error EAS made');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'numberCharactersPerLine', 'The number of characters per line exceeds the max length of the text');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'gapBetweenPreviousSubtitle', 'The gap between previous subtitle is less than allowed in the settings');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'exceedTextLength', 'The number of characters per line exceeds the max length of the text more then 20%');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'notLatin', 'Some characters are not LATIN/UNICODE');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'doubleSpace', 'Double space of the text');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'notTrimmedText', 'Space at beginning or end of the text');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'noPunctuation', 'No punctuation at the end of the line');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'numberLinesSubtitle', 'Max number of lines are exceeded');")
        await queryRunner.query("INSERT INTO `errors` ( `name`, `value`) VALUES ( 'italicFont', 'Font is italic');")
        await queryRunner.query("INSERT INTO `global_settings` ( `name`, `value`) VALUES ( 'minimumDuration', '1');")
        await queryRunner.query("INSERT INTO `global_settings` ( `name`, `value`) VALUES ( 'maximumDuration', '100');")
        await queryRunner.query("INSERT INTO `global_settings` ( `name`, `value`) VALUES ( 'maximumCharactersPerLine', '15');")
        await queryRunner.query("INSERT INTO `global_settings` ( `name`, `value`) VALUES ( 'minimumGapBetweenSubtitles', '5');")
        await queryRunner.query("INSERT INTO `global_settings` ( `name`, `value`) VALUES ( 'readingSpeed', '13');")
        await queryRunner.query("INSERT INTO `global_settings` ( `name`, `value`) VALUES ( 'language', 'english');")
        await queryRunner.query("INSERT INTO `global_settings` ( `name`, `value`) VALUES ( 'maximumQuantityString', '3');")
        await queryRunner.query("INSERT INTO `global_settings` ( `name`, `value`) VALUES ( 'maximumQuantityStringLifting', '3');")
        await queryRunner.query("INSERT INTO `global_settings` ( `name`, `value`) VALUES ( 'useItalicFont', 'true');")
    }

   public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DELETE FROM errors;")
        await queryRunner.query("DELETE FROM global_settings;")
    }

}
