import {MigrationInterface, QueryRunner} from "typeorm";

export class users1566985792188 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `user` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `password_hash` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `name` varchar(255) NULL, `role` enum ('USER', 'ADMIN') NOT NULL DEFAULT 'USER', `status` enum ('ACTIVE', 'INACTIVE', 'FIRED') NOT NULL DEFAULT 'ACTIVE', UNIQUE INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `project` DROP COLUMN `audioFiles`");
        await queryRunner.query("ALTER TABLE `project` ADD `audioFiles` text NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project` DROP COLUMN `audioFiles`");
        await queryRunner.query("ALTER TABLE `project` ADD `audioFiles` mediumtext NOT NULL");
        await queryRunner.query("DROP INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
    }

}
