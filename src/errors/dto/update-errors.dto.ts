import { IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { CreateErrorsDto } from './create-errors.dto';

export class UpdateErrorsDto extends CreateErrorsDto {
  @IsNumber()
  @ApiModelProperty()
  readonly id: number;
}