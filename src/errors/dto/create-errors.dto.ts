import { IsString, IsOptional, ValidateNested, IsEnum } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { UpdateGuideLinesDto } from '../../guidelines/dto/update-guidelines.dto';
import { ERROR_PERMISSIONS } from '../../common/enums/error.permissions.enum';
import { ERROR_COLORS } from '../../common/enums/error.colors.enum';

export class CreateErrorsDto {
  @IsString()
  @ApiModelProperty()
  readonly name: string;

  @IsString()
  @ApiModelProperty()
  readonly value: string;

  @IsEnum(ERROR_PERMISSIONS)
  @IsOptional()
  @ApiModelProperty({ enum: ERROR_PERMISSIONS })
  readonly permissions?: ERROR_PERMISSIONS;

  @IsEnum(ERROR_COLORS)
  @IsOptional()
  @ApiModelProperty({ enum: ERROR_COLORS })
  readonly color?: ERROR_COLORS;

  @IsOptional()
  @ValidateNested()
  @ApiModelProperty({ type: UpdateGuideLinesDto })
  readonly guideline?: UpdateGuideLinesDto;
}