import { Injectable } from '@nestjs/common';
import { Repository, DeleteResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Errors } from './errors.entity';
import { CreateErrorsDto } from './dto/create-errors.dto';
import { UpdateErrorsDto } from './dto/update-errors.dto';

@Injectable()
export class ErrorsService {
  constructor (
    @InjectRepository(Errors)
    private readonly errorsRepository: Repository<Errors>,
  ) { }

  create(createErrorsDto: CreateErrorsDto[]): Promise<Errors[]> {
    const error = [];
    for (let i = 0; i < createErrorsDto.length; i++) {
      error.push({ ...new Errors(), ...createErrorsDto[i] });
    }
    return this.errorsRepository.save(error);
  }

  async fetchAll(): Promise<CreateErrorsDto[]> {
    return this.errorsRepository.find();
  }

  findById(id: string | number): Promise<Errors> {
    return this.errorsRepository.findOne(id, { relations: ['guideline']});
  }

  findByName(name: string): Promise<Errors> {
    return this.errorsRepository.findOne({ name });
  }

  async updateById(id: string | number, updateErrorsDto: UpdateErrorsDto): Promise<Errors> {
    await this.errorsRepository.update(id, updateErrorsDto);
    return this.findById(id);
  }

  async removeById(id: number | string): Promise<DeleteResult> {
    return this.errorsRepository.delete(id);
  }
}