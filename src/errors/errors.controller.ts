import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  HttpCode,
  UseGuards,
} from '@nestjs/common';
import { Errors } from './errors.entity';
import { ErrorsService } from './errors.service';
import { CreateErrorsDto } from './dto/create-errors.dto';
import { UpdateErrorsDto } from './dto/update-errors.dto';
import { DeleteResult } from 'typeorm';
import { ApiOperation, ApiUseTags, ApiResponse, ApiImplicitParam, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/roles.guard';
import { UserRole } from '../common/enums/user-role.enum';
import { AcceptableRoles } from '../common/decorators/roles.decorator';

@Controller('errors')
@ApiUseTags('errors')
@UseGuards(AuthGuard('jwt'), RolesGuard)
@ApiBearerAuth()
export class ErrorsController {
  constructor(
    private readonly errorsService: ErrorsService,
  ) { }

  @Get()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Get all errors' })
  async fetchAll(): Promise<CreateErrorsDto[]> {
    return this.errorsService.fetchAll();
  }

  @Get('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Get error by id' })
  @ApiImplicitParam({ name: 'id' })
  fetchById(@Param('id') id: number | string): Promise<Errors> {
    return this.errorsService.findById(id);
  }

  @Post()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Create a new error' })
  async create(@Body() createErrorsDto: CreateErrorsDto[]): Promise<Errors[]> {
    return this.errorsService.create(createErrorsDto);
  }

  @Put('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Update error by id' })
  @ApiImplicitParam({ name: 'id' })
  update(@Param('id') id: string | number, @Body() updateErrorsDto: UpdateErrorsDto): Promise<Errors> {
    return this.errorsService.updateById(id, updateErrorsDto);
  }

  @Delete('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @HttpCode(204)
  @ApiOperation({ title: 'Delete error by id' })
  @ApiImplicitParam({ name: 'id' })
  @ApiResponse({ status: 204, description: 'Successfully deleted error' })
  remove(@Param('id') id: string | number): Promise<DeleteResult> {
    return this.errorsService.removeById(id);
  }
}