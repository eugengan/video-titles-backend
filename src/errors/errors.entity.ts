import { Entity, Column, ManyToOne } from 'typeorm';
import { BaseEntity } from '../common/entity/base.entity';
import { GuideLines } from '../guidelines/guidelines.entity';
import { ERROR_PERMISSIONS } from '../common/enums/error.permissions.enum';
import { ERROR_COLORS } from '../common/enums/error.colors.enum';

@Entity()
export class Errors extends BaseEntity {
  @Column({ nullable: false })
  name: string;

  @Column({ nullable: false })
  value: string;

  @ManyToOne(type => GuideLines, guideline => guideline.settings)
  guideline: GuideLines;

  @Column({ type: 'enum', enum: ERROR_PERMISSIONS, default: ERROR_PERMISSIONS.DO_NOT_CHECK })
  permissions: ERROR_PERMISSIONS;

  @Column({ type: 'enum', enum: ERROR_COLORS, default: ERROR_COLORS.RED })
  color: ERROR_COLORS;
}