import { HandlebarsAdapter, MailerOptions } from '@nest-modules/mailer';

const {
  EMAIL_USER,
  EMAIL_PASSWORD,
  EMAIL_DOMAIN,
  EMAIL_DOMAIN_PORT,
} = process.env;

export const EMAIL_CONFIG = {
  transport: {
    host: EMAIL_DOMAIN,
    port: EMAIL_DOMAIN_PORT,
    secure: false,
    auth: {
      user: EMAIL_USER,
      pass: EMAIL_PASSWORD,
    },
  },
  template: {
    dir: process.env.PWD + '/src/templates',
    adapter: new HandlebarsAdapter(),
    options: {
      strict: true,
    },
  },
} as MailerOptions;