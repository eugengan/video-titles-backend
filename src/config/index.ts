const {
  DB_HOST,
  DB_PORT,
  DB_USER,
  DB_PASSWORD,
  DB_NAME,
  LOG_LEVEL,
  JWT_SECRET,
  JWT_EXPIRES,
} = process.env;
import { ORM_CONFIG } from './orm.config';

export { EMAIL_CONFIG } from './email.config';

export {
  DB_HOST,
  DB_PORT,
  DB_USER,
  DB_PASSWORD,
  DB_NAME,
  LOG_LEVEL,
  ORM_CONFIG,
  JWT_SECRET,
  JWT_EXPIRES,
};
