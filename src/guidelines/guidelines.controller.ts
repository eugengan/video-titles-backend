import {
  Controller,
  UseGuards,
  Get,
  Param,
  Post,
  Body,
  HttpStatus,
  HttpException,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth, ApiOperation, ApiResponse, ApiImplicitParam } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { GuideLinesService } from './guidelines.service';
import { GuideLines } from './guidelines.entity';
import { ResponseGuideLinesDto } from './dto/response-guidelines.dto';
import { CreateGuideLinesDto } from './dto/create-guidelines.dto';
import { UpdateGuideLinesDto } from './dto/update-guidelines.dto';
import { FilterGuideLinesDto } from './dto/filter-guidelines-type.dto';
import { RolesGuard } from '../common/guards/roles.guard';
import { UserRole } from '../common/enums/user-role.enum';
import { AcceptableRoles } from '../common/decorators/roles.decorator';
import { DeleteResponseDto } from 'common/dto/delete.response.dto';

@Controller('guidelines')
@ApiUseTags('guidelines')
@UseGuards(AuthGuard('jwt'), RolesGuard)
@ApiBearerAuth()
export class GuideLinesController {

  constructor(
    private readonly guideLinesService: GuideLinesService,
  ){}

  @Get()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Get all guidelines' })
  @ApiResponse({ status: 200, type: ResponseGuideLinesDto, isArray: true })
  fetchAll(@Query() query: FilterGuideLinesDto): Promise<GuideLines[]> {
    return this.guideLinesService.findAll(query);
  }

  @Get('/:id')
  @ApiOperation({ title: 'Get guideline by id' })
  @ApiResponse({ status: 200, type: ResponseGuideLinesDto })
  @ApiImplicitParam({ name: 'id' })
  fetchById(@Param('id') id: number): Promise<GuideLines> {
    return this.guideLinesService.findById(id);
  }

  @Post()
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Create a new guidelines' })
  @ApiResponse({ status: 200, type: ResponseGuideLinesDto })
  async create(@Body() createGuideLine: CreateGuideLinesDto): Promise<GuideLines> {
    if (await this.guideLinesService.isNotUniq(createGuideLine.name)) {
      throw new HttpException('Guideline with this name already exists', HttpStatus.BAD_REQUEST);
    }
    return this.guideLinesService.create(createGuideLine);
  }

  @Put('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Update guideline by id' })
  @ApiResponse({ status: 200, type: ResponseGuideLinesDto })
  @ApiImplicitParam({ name: 'id' })
  async update(@Param('id') id: number, @Body() updateGuideline: UpdateGuideLinesDto): Promise<GuideLines> {
    const exists = await this.guideLinesService.checkExistenceById(id);
    if (!exists) {
      throw new HttpException('Guidelines type not found', HttpStatus.NOT_FOUND);
    }

    return this.guideLinesService.updateById(id, updateGuideline);
  }

  @Delete('/:id')
  @AcceptableRoles(UserRole.ADMIN)
  @ApiOperation({ title: 'Delete guideline by id' })
  @ApiResponse({ status: 204, description: 'Successfully deleted guideline'})
  @ApiImplicitParam({ name: 'id' })
  async remove(@Param('id') id: number): Promise<DeleteResponseDto>  {
    await this.guideLinesService.removeById(id);
    return { success: true, id };
  }
}
