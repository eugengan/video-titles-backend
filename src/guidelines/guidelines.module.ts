import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GuideLinesService } from './guidelines.service';
import { GuideLinesController } from './guidelines.controller';
import { GuideLines } from './guidelines.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([GuideLines]),
  ],
  controllers: [GuideLinesController],
  providers: [GuideLinesService],
  exports: [GuideLinesService],
})
export class GuidelinesModule {}
