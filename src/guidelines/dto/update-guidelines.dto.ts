import { IsNumber } from 'class-validator';
import { CreateGuideLinesDto } from './create-guidelines.dto';

export class UpdateGuideLinesDto extends CreateGuideLinesDto {
  @IsNumber() readonly id?: number;
}