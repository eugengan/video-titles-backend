import { IsString, IsOptional, ValidateNested } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { CreateErrorsDto } from '../../errors/dto/create-errors.dto';
import { CreateSettingsDto } from '../../settings/dto/create-settings.dto';

export class CreateGuideLinesDto {
  @IsString()
  @ApiModelProperty()
  readonly name: string;

  @IsOptional()
  @ValidateNested()
  @ApiModelProperty({ type: CreateErrorsDto })
  readonly errors?: CreateErrorsDto[];

  @IsOptional()
  @ValidateNested()
  @ApiModelProperty({ type: CreateErrorsDto })
  readonly settings?: CreateSettingsDto[];
}
