import { ApiModelProperty } from '@nestjs/swagger';
import { BaseResponseDto } from '../../common/dto/base.response.dto';
import { Settings } from '../../settings/settings.entity';
import { Project } from '../../project/project.entity';

export class ResponseGuideLinesDto extends BaseResponseDto {
  @ApiModelProperty({ example: 'John' })
  readonly name: string;

  @ApiModelProperty({ type: Settings, isArray: true })
  readonly settings: Settings[];

  @ApiModelProperty({ type: Project, isArray: true })
  readonly projects: Project[];
}