import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { getManager, Repository } from 'typeorm';

import { QueryOrder } from '../common/enums/query-order.enum';
import { FilterGuideLinesDto } from './dto/filter-guidelines-type.dto';

import { GuideLines } from './guidelines.entity';
import { CreateGuideLinesDto } from './dto/create-guidelines.dto';
import { UpdateGuideLinesDto } from './dto/update-guidelines.dto';
import { Errors } from '../errors/errors.entity';
import { Settings } from '../settings/settings.entity';

const relations = {
  relations: ['errors', 'settings'],
};

@Injectable()
export class GuideLinesService {

  constructor(
    @InjectRepository(GuideLines)
    private readonly guidelines: Repository<GuideLines>,
  ){}

  findAll(query: FilterGuideLinesDto): Promise<GuideLines[]> {
    const { name, orderType = QueryOrder.DESC, orderBy = 'name' } = query;
    const pattern = `%${name}%`;

    const sqlQuery = this.guidelines.createQueryBuilder('guideline');

    if (name) {
      sqlQuery.andWhere('guideline.name LIKE :name',
        { name: pattern });
    }
    const order = {};
    order[`guideline.${orderBy}`] = orderType || QueryOrder.DESC;

    return sqlQuery
      .leftJoinAndSelect('guideline.settings', 'settings')
      .leftJoinAndSelect('guideline.errors', 'errors')
      .orderBy(order)
      .getMany();
  }

  findById(id: number): Promise<GuideLines> {
    return this.guidelines.findOneOrFail({ id }, { ...relations });
  }

  async create(createGuideLine: CreateGuideLinesDto): Promise<GuideLines> {
    let guideline: GuideLines;
    await getManager().transaction(async transactionalEntityManager => {
      guideline = await transactionalEntityManager.save(GuideLines, {
        ...new GuideLines(),
        ...createGuideLine,
      } as GuideLines);

      const settings = createGuideLine.settings.map(i => ({
        ...new Settings(),
        name: i.name,
        value: i.value,
        guideline: { id: guideline.id },
      } as Settings));
      await transactionalEntityManager.insert(Settings, settings);

      const errors =  createGuideLine.errors.map(i => ({
        ...new Errors(),
        name: i.name,
        value: i.value,
        color: i.color,
        permissions: i.permissions,
        guideline: { id: guideline.id },
      } as Errors));

      await transactionalEntityManager.insert(Errors, errors);
    });
    return this.findById(guideline.id);
  }

  async checkExistenceById(id: number): Promise<boolean> {
    const count = await this.guidelines.count({ id });
    return !!count;
  }

  async isNotUniq(name: string, id?: number): Promise<boolean> {
    const guidelineWithThisName = await this.guidelines.findOne({ name });
    if (!guidelineWithThisName) {
      return false;
    }
    if (id) {
      return guidelineWithThisName.id !== id && guidelineWithThisName.name === name;
    }
    return !!guidelineWithThisName;
  }

  async updateById(id: number, updateGuideLine: UpdateGuideLinesDto): Promise<GuideLines> {
    await getManager().transaction(async transactionalEntityManager => {
      await transactionalEntityManager.update(GuideLines, id, {
        ...new GuideLines(),
        ...updateGuideLine,
      } as GuideLines);
      const promises = [];
      for (const setting of updateGuideLine.settings) {
        const s = {...new Settings(), ...setting, guideline: { id }};
        promises.push(transactionalEntityManager.update(Settings, s.id, s));
      }
      for (const error of updateGuideLine.errors) {
        const e = {...new Errors(), ...error, guideline: { id }};
        promises.push(transactionalEntityManager.update(Errors, e.id, e));
      }
    });
    return this.findById(id);
  }

  async removeById(id: number): Promise<void> {
    await getManager().transaction(async transactionalEntityManager => {
      await transactionalEntityManager.delete(Errors, { guideline: { id }});
      await transactionalEntityManager.delete(Settings, { guideline: { id }});
      await transactionalEntityManager.delete(GuideLines, id);
    });
  }
}
