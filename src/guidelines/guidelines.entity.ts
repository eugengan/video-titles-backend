import { Column, OneToMany, Entity } from 'typeorm';
import { BaseEntity } from '../common/entity/base.entity';
import { Settings } from '../settings/settings.entity';
import { Errors } from '../errors/errors.entity';

@Entity()
export class GuideLines extends BaseEntity{
  @Column({ nullable: false, unique: true })
  name: string;

  @OneToMany(type => Settings, settings => settings.guideline)
  settings?: Settings[];

  @OneToMany(type => Errors, error => error.guideline)
  errors?: Errors[];
}
